# online-exam

#### 介绍
毕设——在线考试系统

#### 软件架构
 **后端：** Springboot+MyBatis
 **前端：** HTML + CSS + jquery + Bootstrap +Thymeleaf
 **数据库: ** MySQL


#### 安装教程

系统调试环境
    开发工具：Intellij IDEA
    操作系统：Windoows 10
    数据库：MySQL 8.0.16及以上
    服务器：Tomcat 9.0 
    JDK：JDK11 及以上
    浏览器：Google Chrome


#### 使用说明
1、找到exam_online_test.sql文件，在MySQL 图形工具里执行添加数据
2、修改application.properties中的mysql访问账号和密码
3、直接运行SpringBootApplication中的main

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
