function showAlert(text,time) {
    $('#alertMessage').text(text)
    $('#alertMsgModal').modal('show')
    setTimeout(function () {
        $('#alertMsgModal').modal('hide')
    },time*1000)
}

function showStuList(list) {
    $('#stuListTbody').empty();
    var count = 1;
    console.log(list.length)
    for (var i = 0;i<list.length;i++) {
        var tr = $("<tr></tr>");
        tr.append("<th scope='row' colspan='1'>" + count + "</th>")
        tr.append("<td colspan='2'>" + list[i].userName + "</td>")
        tr.append("<td colspan=\"2\">" +list[i].password+ "</td>\n")
        tr.append("<td colspan=\"3\">" + list[i].major + "</td>")
        tr.append("<td colspan=\"1\">" + list[i].grade + "</td>")
        tr.append("<td colspan=\"1\">" + list[i].stuClass + "</td>")
        tr.append("<td colspan=\"1\">" + list[i].name + "</td>")
        //两个按钮
        var btnTd = $("<td colspan='3'></td>");
        var btnDiv = $("<div class='btn-group'></div>")
        var btnDel = $("<button type=\"button\" class=\"btn student-delete btn-link\" data-toggle=\"tooltip\" data-placement=\"right\" title='删除' id='stuDelete" + list[i].id + "'></button>")
        var btnDelTag = $("<i class=\"bi bi-trash\"></i>")
        btnDel.append(btnDelTag)
        btnDiv.append(btnDel)
        var btnUp = $("<button type=\"button\" class=\"btn student-update btn-link\" data-toggle=\"tooltip\" data-placement=\"right\"  title='修改' id='stuUpdate" + list[i].id + "'></button>")
        var btnUpTag = $("<i class=\"bi bi-pencil\"></i>")
        btnUp.append(btnUpTag)
        btnDiv.append(btnUp)
        if(list[i].status == 'yes'){
            console.log(list[i].status)
            var btnForbid = $("<button type=\"button\" class=\"btn student-forbid btn-link\" data-toggle=\"tooltip\" data-placement=\"right\"  title='禁用' id='stuForbid" + list[i].id + "'>禁用</button>")
        }else {
            var btnForbid = $("<button type=\"button\" class=\"btn student-forbid btn-link\" data-toggle=\"tooltip\" data-placement=\"right\"  title='解禁' id='stuForbid" + list[i].id + "'>解禁</button>")
        }
        btnDiv.append(btnForbid)
        btnTd.append(btnDiv)
        tr.append(btnTd)

        $('#stuListTbody').append(tr)
        count++;
    }
}

function refreshStudentInfo(majors,grades,classes) {
    $('.info-select').empty()
    $('#stuMajorSelect').append($("<option>全部</option>"))
    $('#stuGradeSelect').append($("<option>全部</option>"))
    $('#stuClassSelect').append($("<option>全部</option>"))
    for (var i = 0;i < majors.length;i++){
        $('#stuMajorSelect').append($("<option>" +majors[i]+ "</option>"))
        $('#newStuMajorSelect').append($("<a class='dropdown-item' onclick='setValue(this,\'newStudentMajor\')' href='#'>" +majors[i]+ "</a>"))
    }
    for (var i = 0;i < grades.length;i++){
        $('#stuGradeSelect').append($("<option>" +grades[i]+ "</option>"))
        $('#newStuGradeSelect').append($("<a class='dropdown-item' onclick='setValue(this,\'newStudentGrade\')' href='#'>" +grades[i]+ "</a>"))
    }
    for (var i = 0;i < classes.length;i++){
        $('#stuClassSelect').append($("<option>" +classes[i]+ "</option>"))
        $('#newStuClassSelect').append($("<a class='dropdown-item' onclick='setValue(this,\'newStudentClass\')' href='#'>" +classes[i]+ "</a>"))
    }

}

/*教师列表展示*/
function showTeacherList(list) {
    $('#teacherListTbody').empty();
    var count = 1;
    for (var i = 0;i<list.length;i++) {
        var tr = $("<tr></tr>");
        tr.append("<th scope='row' colspan='1'>" + count + "</th>")
        tr.append("<td colspan='2'>" + list[i].userName + "</td>")
        tr.append("<td colspan=\"1\">" +list[i].name+ "</td>\n")
        tr.append("<td colspan=\"2\">" + list[i].password + "</td>")
        var btnTd = $("<td colspan='3'></td>");
        var btnDiv = $("<div class='btn-group'></div>")
        var btnDel = $("<button type=\"button\" class=\"btn te-delete btn-link\" data-toggle=\"tooltip\" data-placement=\"right\" title='删除' id='teacherDelete" + list[i].id + "'></button>")
        var btnDelTag = $("<i class=\"bi bi-trash\"></i>")
        btnDel.append(btnDelTag)
        btnDiv.append(btnDel)
        var btnUp = $("<button type=\"button\" class=\"btn te-update btn-link\" data-toggle=\"tooltip\" data-placement=\"right\"  title='修改' id='teacherUpdate" + list[i].id + "'></button>")
        var btnUpTag = $("<i class=\"bi bi-pencil\"></i>")
        btnUp.append(btnUpTag)
        btnDiv.append(btnUp)
        if(list[i].status == 'yes'){
            console.log(list[i].status)
            var btnForbid = $("<button type=\"button\" class=\"btn student-forbid btn-link\" data-toggle=\"tooltip\" data-placement=\"right\"  title='禁用' id='teacherForbid" + list[i].id + "'>禁用</button>")
        }else {
            var btnForbid = $("<button type=\"button\" class=\"btn student-forbid btn-link\" data-toggle=\"tooltip\" data-placement=\"right\"  title='解禁' id='teacherForbid" + list[i].id + "'>解禁</button>")
        }
        btnDiv.append(btnForbid)
        btnTd.append(btnDiv)
        tr.append(btnTd)

        $('#teacherListTbody').append(tr)
        count++;
    }
}

/*日志列表展示*/
function showLogList(list) {
    $('#logListTbody').empty();
    console.log(list)
    var count = 1;
    for (var i = 0;i<list.length;i++) {
        var tr = $("<tr></tr>");
        tr.append("<th scope='row' colspan='1'>" + count + "</th>")
        tr.append("<td colspan='1'>" + list[i].operator + "</td>")
        tr.append("<td colspan=\"3\">" +list[i].operation+ "</td>\n")
        var date = new Date(list[i].time)
        var year = date.getFullYear()+ '-';
        var month = (date.getUTCMonth()+1 < 10 ? '0' +(date.getUTCMonth()+1) : (date.getUTCMonth()+1))+ '-';
        var day = (date.getDate() < 10 ? '0' +date.getDate() : date.getDate());
        var h = (date.getHours() < 10 ? '0' + date.getHours() : date.getHours()) + ':';
        var m = (date.getMinutes() < 10 ? '0' + date.getMinutes() : date.getMinutes()) + ':';
        var s = (date.getSeconds() < 10 ? '0' + date.getSeconds() : date.getSeconds());
        var nowTime = year+month+day+" "+h+m+s;
        tr.append("<td colspan=\"3\">" + nowTime + "</td>")
        var btnTd = $("<td colspan='1'></td>");
        var btnDiv = $("<div class='btn-group'></div>")
        var btnDel = $("<button type=\"button\" class=\"btn log-delete btn-link\" data-toggle=\"tooltip\" data-placement=\"right\" title='删除' id='logDelete" + list[i].id + "'></button>")
        var btnDelTag = $("<i class=\"bi bi-trash\"></i>")
        btnDel.append(btnDelTag)
        btnDiv.append(btnDel)
        btnTd.append(btnDiv)
        tr.append(btnTd)

        $('#logListTbody').append(tr)
        count++;
    }
}