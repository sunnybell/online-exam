function postLink(path){
    event.preventDefault();
    var form = document.createElement("form");
    form.method = "post";
    form.action = path;
    document.body.appendChild(form);
    form.submit();
    document.body.removeChild(form);
}

function post(path) {
    var form = document.createElement("form");
    form.method = "post";
    form.action = path;
    document.body.appendChild(form);
    form.submit();
    document.body.removeChild(form);
}

$(function () {
    $('[data-toggle="tooltip"]').tooltip();
})

/*数据交互后提示框显示*/
function showAlertModal(msg) {
    // console.log(msg)
    $('#alertMessage').text(msg);
    $('#alertErrorModal').modal('show');
    setTimeout(function () {
        $('#alertErrorModal').modal('hide');
    },2000);
}

function showMadal(msg) {
    $('#alertMessage').text(msg);
    $('#alertErrorModal').modal('show');
}

function hideModal() {
    $('#alertErrorModal').modal('hide');
}

function StandardPost(url,name,data){
    var form = $("<form method='post'></form>"),
        input;
    form.attr({"action":url});
    //组合成一个input
    input = $("<input type='hidden'>");
    input.attr({"name":name});
    input.val(data);
    form.append(input);
    $(document.body).append(form);
    form.submit();  //进行提交
}

//清除所有cookie函数
function clearAllCookie() {
    var date=new Date();
    date.setTime(date.getTime()-10000);
    //keys为所要删除cookie的名字
    var keys=document.cookie.match(/[^ =;]+(?=\=)/g);
    //删除所有cookie
    if (keys) {
        for (var i =  keys.length; i--;)
            document.cookie=keys[i]+"=0; expire="+date.toGMTString()+"; path=/";
    }
}
