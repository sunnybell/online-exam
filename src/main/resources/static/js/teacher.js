function refreshSubjectsDropDown(subjects) {
    $('.subject-dropdown').empty()
    $('#questionSubScreen').append($('<option>全部</option>'))
    $('#testSubjectScreen').append($('<option>全部</option>'))
    $('#testSubjectDropDown')
    for (var i = 0;i < subjects.length;i++){
        $('#newQuestionDropDown').append($("<a class='dropdown-item' onclick='selectValue(this)' href='#'>" +subjects[i]+ "</a>"));
        $('#questionSubScreen').append($("<option>" +subjects[i]+ "</option>"))
        $('#testSubjectScreen').append($("<option>" +subjects[i]+ "</option>"))
        $('#testSubjectDropDown').append($("<a class=\"dropdown-item\" href=\"#\" onclick=\"selectTestSubject(this)\" >" +subjects[i]+ "</a>"))
    }
}

/*试题列表展示*/
function showQuestionList(list){
    $('#questionTbody').empty();
    var count = 1;
    for (var i = 0;i<list.length;i++){
        var tr = $("<tr></tr>");
        tr.append("<th scope='row' colspan='1'>" +count+ "</th>")
        tr.append("<td colspan='1'>" +list[i].catagory+ "</td>")
        tr.append("<td colspan='4' data-toggle='tooltip' data-placement='right' title='" +list[i].title+ "'>" +list[i].title+ "</td>")
        tr.append("<td colspan='2' data-toggle='tooltip' data-placement='right' title='" +list[i].answer+ "'>" +list[i].answer+ "</td>")
        tr.append("<td colspan='3'>" +list[i].subject+ "</td>")
        tr.append("<td colspan='1'>" +list[i].difficulty+ "</td>")
        //两个按钮
        var btnTd = $("<td colspan='2'></td>");
        var btnDiv = $("<div class='btn-group'></div>")
        var btnDel = $("<button type=\"button\" class=\"btn question-delete btn-link\" data-toggle=\"tooltip\" data-placement=\"right\" title=\"删除\" id='deleteQuestionBtn" +list[i].id+ "'></button>")
        var btnDelTag = $("<i class=\"bi bi-trash\"></i>")
        btnDel.append(btnDelTag)
        btnDiv.append(btnDel)
        var btnUp = $("<button type=\"button\" class=\"btn question-update btn-link\" data-toggle=\"tooltip\" data-placement=\"right\" title=\"修改\" id='updateQuestionBtn" +list[i].id+ "'></button>")
        var btnUpTag = $("<i class=\"bi bi-pencil-square\"></i>")
        btnUp.append(btnUpTag)
        btnDiv.append(btnUp)
        btnTd.append(btnDiv)
        tr.append(btnTd)
        $('#questionTbody').append(tr)
        count++;
    }
}

/*考试列表展示*/
function showTestList(list) {
    $('#testTbody').empty();
    var count = 1;
    for (var i = 0;i<list.length;i++){
        var tr = $("<tr class=\"testTr\"></tr>");
        tr.append("<th scope='row' colspan='1'>" +count+ "</th>")
        tr.append("<td colspan='2'>" +list[i].subject+ "</td>")
        tr.append("<td colspan=\"3\" class=\"test-title\">" +list[i].title+ "</td>")
        tr.append("<td colspan=\"3\">" +list[i].start+ "</td>")
        tr.append("<td colspan=\"3\">" +list[i].end+ "</td>")
        tr.append("<td colspan=\"1\">" +list[i].questionNum+ "</td>")
        tr.append("<td colspan=\"1\">" +list[i].score+ "</td>")
        //两个按钮
        var btnTd = $("<td colspan='2'></td>");
        var btnDiv = $("<div class='btn-group'></div>")
        var btnDel = $("<button type=\"button\" class=\"btn test-delete btn-link\" data-toggle=\"tooltip\" data-placement=\"right\" data-original-title=\"删除\" title='' id='testDeleteBtn" +list[i].id+ "'></button>")
        var btnDelTag = $("<i class=\"bi bi-trash\"></i>")
        btnDel.append(btnDelTag)
        btnDiv.append(btnDel)
        var btnUp = $("<button type=\"button\" class=\"btn test-check btn-link\" data-toggle=\"tooltip\" data-placement=\"right\" data-original-title=\"详情\" title='' id='testCheckBtn" +list[i].id+ "'></button>")
        var btnUpTag = $("<i class=\"bi bi-eye\"></i>")
        btnUp.append(btnUpTag)
        btnDiv.append(btnUp)
        btnTd.append(btnDiv)
        tr.append(btnTd)

        $('#testTbody').append(tr)
        count++;
    }
}

/*试卷学生列表*/
function showStuList(list) {
    $('#stuScoreTbody').empty();
    for (var i = 0;i<list.length;i++){
        var tr = $("<tr></tr>");
        tr.append("<td colspan='2'>" +list[i].username+ "</td>")
        tr.append("<td colspan='2'>" +list[i].name+ "</td>")
        tr.append("<td colspan=\"3\">" +list[i].major+ "</td>")
        tr.append("<td colspan=\"1\">" +list[i].grade+ "</td>")
        tr.append("<td colspan=\"1\">" +list[i].stuClass+ "</td>")
        tr.append("<td colspan=\"1\">" +list[i].studentScore+ "</td>")
        //两个按钮
        var btnTd = $("<td colspan='2'></td>");
        var btnDiv = $("<div class='btn-group'></div>")
        var btnDel = $("<button type=\"button\" class=\"btn stu-score-update btn-link\" data-toggle=\"tooltip\" data-placement=\"right\" title=\"批阅\" id='" +list[i].username+ "'></button>")
        var btnDelTag = $("<i class=\"bi bi-pencil\"></i>")
        btnDel.append(btnDelTag)
        btnDiv.append(btnDel)
        btnTd.append(btnDiv)
        tr.append(btnTd)
        $('#stuScoreTbody').append(tr)
        // console.log(tr)
    }
}
