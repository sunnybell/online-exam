package com.zx.exam.entity.requestbody;


import lombok.Data;

import java.util.List;

/**
 * @ClassName UpdateQuestion
 * @Author Sunny
 * @Date 2021年05月05日
 * @Package com.zx.exam.entity.requestbody
 * @Description TODO
 */
@Data
public class UpdateQuestion{
    String id;
    String title;
    String subject;
    String catagory;
    String difficulty;
    String answer;
    List<Option> options;
}
