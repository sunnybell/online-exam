package com.zx.exam.entity.requestbody;


import lombok.Data;

import java.util.List;
import java.util.Map;

/**
 * @ClassName DeclareNewPaperRequestBody
 * @Author Sunny
 * @Date 2021年05月12日
 * @Package com.zx.exam.entity.requestbody
 * @Description TODO
 */
@Data
public class DeclareNewPaperRequestBody {
    String title;
    String start;
    String end;
    String subject;
    String totalScore;
    String passScore;
    List<Map<String,String>> questionParam;
}
