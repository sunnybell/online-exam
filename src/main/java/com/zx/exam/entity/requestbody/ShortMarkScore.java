package com.zx.exam.entity.requestbody;


import lombok.Data;

/**
 * @ClassName ShortMarkScore
 * @Author Sunny
 * @Date 2021年05月16日
 * @Package com.zx.exam.entity.requestbody
 * @Description TODO
 */
@Data
public class ShortMarkScore {
    String id;
    String score;
}
