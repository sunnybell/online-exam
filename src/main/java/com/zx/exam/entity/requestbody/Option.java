package com.zx.exam.entity.requestbody;


import lombok.Data;

/**
 * @ClassName Option
 * @Author Sunny
 * @Date 2021年04月28日
 * @Package com.zx.exam.entity.requestbody
 * @Description TODO
 */
@Data
public class Option {
    String name;
    String content;
}
