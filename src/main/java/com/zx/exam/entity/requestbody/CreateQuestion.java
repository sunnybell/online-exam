package com.zx.exam.entity.requestbody;


import lombok.Data;

import java.util.List;

/**
 * @ClassName CeateQuestion
 * @Author Sunny
 * @Date 2021年04月28日
 * @Package com.zx.exam.entity.requestbody
 * @Description TODO
 */
@Data
public class CreateQuestion {
    String title;
    String subject;
    String catagory;
    String difficulty;
    String answer;
    List<Option> options;
}
