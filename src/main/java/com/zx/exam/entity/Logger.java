package com.zx.exam.entity;


import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.Date;

/**
 * @ClassName Logger
 * @Author Sunny
 * @Date 2021年04月18日
 * @Package com.zx.exam.entity
 * @Description TODO
 */
@Data
@AllArgsConstructor
public class Logger {
    Integer id;
    String operator;
    String operation;
    Date time;
}
