package com.zx.exam.entity;


import lombok.Data;

/**
 * @ClassName PaperQuestion
 * @Author Sunny
 * @Date 2021年05月14日
 * @Package com.zx.exam.entity
 * @Description TODO
 */
@Data
public class PaperQuestion {
    Integer id;
    Integer index;
    Integer testpaperId;
    Integer questionId;
    String fullScore;
    String score;
    String studentAnswer;
    String comment;
}
