package com.zx.exam.entity;


import lombok.Data;

/**
 * @ClassName Subject
 * @Author Sunny
 * @Date 2021年04月27日
 * @Package com.zx.exam.entity
 * @Description TODO
 */
@Data
public class Subject {
    Integer id;
    String name;
}
