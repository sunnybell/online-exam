package com.zx.exam.entity;


import lombok.Data;

/**
 * @ClassName Administrator
 * @Author Sunny
 * @Date 2021年04月11日
 * @Package com.zx.exam.entity
 * @Description TODO
 */
@Data
public class Administrator {

    private Integer id;
    private String userName;
    private String password;

}
