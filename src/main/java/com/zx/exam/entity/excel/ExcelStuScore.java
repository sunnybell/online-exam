package com.zx.exam.entity.excel;


import com.alibaba.excel.annotation.ExcelProperty;
import lombok.Data;

/**
 * @ClassName ExcelStuScore
 * @Author Sunny
 * @Date 2021年05月17日
 * @Package com.zx.exam.entity.excel
 * @Description TODO
 */
@Data
public class ExcelStuScore {
    @ExcelProperty("学号")
    String username;

    @ExcelProperty("学生姓名")
    String name;

    @ExcelProperty("专业")
    String major;

    @ExcelProperty("年级")
    String grade;

    @ExcelProperty("班级")
    String stuClass;

    @ExcelProperty("教师")
    String teacher;

    @ExcelProperty("科目")
    String subject;

    @ExcelProperty("成绩")
    String studentScore;

    @ExcelProperty("及格分数")
    String passScore;

    @ExcelProperty("试卷总分")
    String sumScore;

}
