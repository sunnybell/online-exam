package com.zx.exam.entity.excel;


import com.alibaba.excel.annotation.ExcelProperty;
import lombok.Data;

/**
 * @ClassName ExcelQuestionModel
 * @Author Sunny
 * @Date 2021年05月05日
 * @Package com.zx.exam.entity.excel
 * @Description TODO
 */
@Data
public class ExcelQuestionModel {
    @ExcelProperty(value = "题型",index = 0)
    String catagory;
    @ExcelProperty(value = "题目",index = 1)
    String title;
    @ExcelProperty(value = "A",index = 2)
    String optionA;
    @ExcelProperty(value = "B",index = 3)
    String optionB;
    @ExcelProperty(value = "C",index = 4)
    String optionC;
    @ExcelProperty(value = "D",index = 5)
    String optionD;
    @ExcelProperty(value = "答案",index = 6)
    String answer;
    @ExcelProperty(value = "科目",index = 7)
    String subject;
    @ExcelProperty(value = "难度",index = 8)
    String difficulty;

}
