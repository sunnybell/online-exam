package com.zx.exam.entity.excel;


import com.alibaba.excel.annotation.ExcelProperty;
import lombok.Data;

/**
 * @ClassName ExcelStudent
 * @Author Sunny
 * @Date 2021年05月20日
 * @Package com.zx.exam.entity.excel
 * @Description TODO
 */
@Data
public class ExcelStudent {
    @ExcelProperty("学号")
    String username;
    @ExcelProperty("密码")
    String password;
    @ExcelProperty("专业")
    String major;
    @ExcelProperty("年级")
    String grade;
    @ExcelProperty("班级")
    String stuClass;
    @ExcelProperty("姓名")
    String name;
}
