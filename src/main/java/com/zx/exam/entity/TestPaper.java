package com.zx.exam.entity;


import lombok.Data;

import java.util.Date;
import java.sql.Timestamp;

/**
 * @ClassName TestPaper
 * @Author Sunny
 * @Date 2021年05月12日
 * @Package com.zx.exam.entity
 * @Description TODO
 */
@Data
public class TestPaper {
    Integer id;
    String title;
    Date startTime;
    Date endTime;
    Date submitTime;
    Integer studentId;
    String teacher;
    String passScore;
    String sumScore;
    String subject;
    String studentScore;
}
