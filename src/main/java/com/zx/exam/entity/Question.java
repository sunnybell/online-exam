package com.zx.exam.entity;


import lombok.Data;

/**
 * @ClassName Question
 * @Author Sunny
 * @Date 2021年04月27日
 * @Package com.zx.exam.entity
 * @Description TODO
 */
@Data
public class Question {
    Integer id;
    String title;
    String subject;
    String catagory;
    String difficulty;
    String answer;
    String optionA;
    String optionB;
    String optionC;
    String optionD;

}
