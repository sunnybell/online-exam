package com.zx.exam.entity.select;


import lombok.Data;

/**
 * @ClassName TestQuestion
 * @Author Sunny
 * @Date 2021年05月19日
 * @Package com.zx.exam.entity.select
 * @Description TODO
 */
@Data
public class TestQuestion {
    Integer paperQuestionId;
    String title;
    String subject;
    String catagory;
    String difficulty;
    String answer;
    String optionA;
    String optionB;
    String optionC;
    String optionD;
}
