package com.zx.exam.entity.select;


import lombok.Data;

/**
 * @ClassName TestPaperInfoQuestion
 * @Author Sunny
 * @Date 2021年05月16日
 * @Package com.zx.exam.entity.select
 * @Description TODO
 */
@Data
public class TestPaperInfoQuestion {
    Integer id;
    String title;
    String catagory;
    String difficulty;
    String answer;
    String optionA;
    String optionB;
    String optionC;
    String optionD;
    String fullScore;
    String score;
    String studentAnswer;
    String comment;
}