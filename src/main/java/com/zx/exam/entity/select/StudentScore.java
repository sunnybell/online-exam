package com.zx.exam.entity.select;


import lombok.Data;

/**
 * @ClassName StudentScore
 * @Author Sunny
 * @Date 2021年05月15日
 * @Package com.zx.exam.entity.select
 * @Description TODO
 */
@Data
public class StudentScore {
/*
* es.username,es.name,es.major,es.grade,es.stu_class,etp.student_score*/
    String username;
    String name;
    String major;
    String grade;
    String stuClass;
    String studentScore;
}
