package com.zx.exam.entity.select;


import lombok.Data;

/**
 * @ClassName DeclaredPaper
 * @Author Sunny
 * @Date 2021年05月15日
 * @Package com.zx.exam.entity.select
 * @Description TODO
 */
@Data
public class DeclaredPaper {
    String subject;
    String title;
    Integer id;
    String teacher;
    String start;
    String end;
    String questionNum;
    String score;
}
