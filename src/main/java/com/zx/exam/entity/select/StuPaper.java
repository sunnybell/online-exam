package com.zx.exam.entity.select;


import lombok.Data;

import java.util.Date;

/**
 * @ClassName StuPaper
 * @Author Sunny
 * @Date 2021年05月17日
 * @Package com.zx.exam.entity.select
 * @Description TODO
 */
@Data
public class StuPaper {
    Integer id;
    String title;
    String score;
    String sumScore;
    String passScore;
    Integer questionCount;
    Integer selectCount;
    Integer judgeCount;
    Integer shorAnswerCount;
    String startTime;
    String endTime;
    String teacherName;
    String status;

}
