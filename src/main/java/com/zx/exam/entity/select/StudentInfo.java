package com.zx.exam.entity.select;


import lombok.Data;

import java.util.List;

/**
 * @ClassName StudentInfo
 * @Author Sunny
 * @Date 2021年05月12日
 * @Package com.zx.exam.entity.select
 * @Description TODO
 */
@Data
public class StudentInfo {
    List<String> majors;
    List<String> grades;
    List<String> classes;
}
