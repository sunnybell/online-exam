package com.zx.exam.entity;


import lombok.Data;

/**
 * @ClassName Teacher
 * @Author Sunny
 * @Date 2021年04月11日
 * @Package com.zx.exam.entity
 * @Description TODO
 */
@Data
public class Teacher {
    private Integer id;
    private String userName;
    private String password;
    private String name;
    private String status;
}
