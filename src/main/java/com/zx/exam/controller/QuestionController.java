package com.zx.exam.controller;


import com.alibaba.fastjson.JSONObject;
import com.zx.exam.entity.Logger;
import com.zx.exam.entity.Question;
import com.zx.exam.entity.Teacher;
import com.zx.exam.entity.requestbody.CreateQuestion;
import com.zx.exam.entity.requestbody.UpdateQuestion;
import com.zx.exam.service.LogService;
import com.zx.exam.service.QuestionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.util.Date;
import java.util.List;

/**
 * @ClassName QuestionController
 * @Author Sunny
 * @Date 2021年04月27日
 * @Package com.zx.exam.controller
 * @Description TODO
 */
@Controller
public class QuestionController {
    @Autowired
    QuestionService questionService;
    @Autowired
    LogService logService;

    static Teacher teacher = null;

    /**新建单个试题
     * @param newQuestion
     * @param
     * @return
     */
    @RequestMapping("createQuestion")
    @ResponseBody
    public JSONObject createQuestion(@RequestBody CreateQuestion newQuestion, HttpServletRequest request){
//        System.out.println(newQuestion);
        teacher = (Teacher)request.getSession().getAttribute("user");
        JSONObject result = new JSONObject();
        result.put("message","添加成功");
        try {
            questionService.addQuestion(newQuestion);
            result.put("list",questionService.getAllQuestion());
            result.put("subjects",questionService.getAllSubjects());
            logService.addLog(new Logger(0,teacher.getName(),"新建试题",new Date()));
        }catch (Exception e){
            result.put("message","添加失败:/n" +e.getMessage());
        }
        return result;
    }

    /**试题导入
     * @param file
     * @return
     */
    @RequestMapping("importQuestion")
    @ResponseBody
    public JSONObject importQuestion(@RequestParam MultipartFile file, HttpServletRequest request) {
        teacher = (Teacher)request.getSession().getAttribute("user");
        JSONObject resultJson = new JSONObject();
        resultJson.put("message","导入成功");
        try {
            questionService.importQuestionsWithStream(file.getInputStream());
            resultJson.put("list",questionService.getAllQuestion());
            resultJson.put("subjects",questionService.getAllSubjects());
            logService.addLog(new Logger(0,teacher.getName(),"导入试题",new Date()));
        } catch (Exception e) {
            resultJson.put("message","导入失败:/n" +e.getMessage());
        }
        return resultJson;
    }

    /*筛选试题*/
    @RequestMapping("screenQuestion")
    @ResponseBody
    public List<Question> screenQuestion( HttpServletRequest request,@RequestParam String catagory, @RequestParam String difficulty, @RequestParam String subject){
        teacher = (Teacher)request.getSession().getAttribute("user");
        logService.addLog(new Logger(0,teacher.getName(),"查询试题",new Date()));
        return questionService.listQuestionByParam(catagory,difficulty,subject);
    }

    /*试题修改*/
    @RequestMapping("updateQuestion")
    @ResponseBody
    public JSONObject updateQuestion(@RequestBody UpdateQuestion updateQuestion ,HttpServletRequest request){
        teacher = (Teacher)request.getSession().getAttribute("user");
        JSONObject result = new JSONObject();
        result.put("message","修改成功");
        try {
            questionService.updateQuestion(updateQuestion);
            result.put("list",questionService.getAllQuestion());
            result.put("subjects",questionService.getAllSubjects());
            logService.addLog(new Logger(0,teacher.getName(),"修改试题",new Date()));
        }catch (Exception e){
            e.printStackTrace();
            result.put("message","修改失败:/n" +e.getMessage());
        }
        return result;
    }

    /**
     * 试题删除
     * @param btnId
     * @return
     */
    @RequestMapping("deleteQuestionById")
    @ResponseBody
    public JSONObject deleteQuestionById(@RequestParam String btnId,HttpServletRequest request){
        teacher = (Teacher)request.getSession().getAttribute("user");
        int deleteQuestionId = Integer.parseInt(btnId.substring("deleteQuestionBtn".length()));
//        System.out.println(btnId);
        JSONObject result = new JSONObject();
        result.put("message","删除成功");
        try {
            questionService.deleteQuestionById(deleteQuestionId);
            result.put("list",questionService.getAllQuestion());
            result.put("subjects",questionService.getAllSubjects());
            logService.addLog(new Logger(0,teacher.getName(),"删除试题",new Date()));
        }catch (Exception e){
            e.printStackTrace();
            result.put("message","删除失败：/n" +e.getMessage());
        }
        return result;
    }

    /**获取对应id的试题
     * @param id
     * @return
     */
    @RequestMapping("questionById")
    @ResponseBody
    public Question questionById(@RequestParam String id){
        Question question = null;
        try {
            question = questionService.getQuestionById(Integer.parseInt(id));
        }catch (Exception e){
            e.printStackTrace();
        }
        return question;
    }

    /*搜索试题*/
    @RequestMapping("searchQuestion")
    @ResponseBody
    public List<Question> searchQuestion(@RequestParam String keyword,HttpServletRequest request){
        teacher = (Teacher)request.getSession().getAttribute("user");
        List<Question> questionSelecteds = questionService.listQuestionByKey(keyword);
        logService.addLog(new Logger(0,teacher.getName(),"查询试题",new Date()));
        return questionSelecteds;
    }

}
