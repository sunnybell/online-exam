package com.zx.exam.controller;


import com.alibaba.fastjson.JSONObject;
import com.zx.exam.common.InfoChangeStatusEnum;
import com.zx.exam.entity.*;
import com.zx.exam.entity.select.StuPaper;
import com.zx.exam.entity.select.TestPaperInfoQuestion;
import com.zx.exam.entity.select.TestQuestion;
import com.zx.exam.service.LogService;
import com.zx.exam.service.QuestionService;
import com.zx.exam.service.StudentService;
import com.zx.exam.service.TestPaperService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * @ClassName StudentController
 * @Author Sunny
 * @Date 2021年04月13日
 * @Package com.zx.exam.controller
 * @Description TODO
 */
@Controller
@RequestMapping("student")
public class StudentController {
    @Autowired
    StudentService studentService;

    @Autowired
    TestPaperService paperService;


//    访问我的资料页面
    @GetMapping("studentInfo")
    public String studentInfo(Model model,HttpServletRequest request){
        model.addAttribute("user",request.getSession().getAttribute("user"));
        return "student/studentInfo";
    }

//    返回考试页面
    @RequestMapping("studentIndex")
    public String studentIndex(Model model,HttpServletRequest request){
        HttpSession session = request.getSession();
        Student student = (Student)session.getAttribute("user");
        model.addAttribute("user",student);
        List<StuPaper> paperList = paperService.listPaperByStudentId(student.getId());
        session.setAttribute("paperList",paperList);
        List<StuPaper> showList = null;
        if (paperList.size() > 3){
            showList = paperList.subList(0,3);
        }else {
            showList = paperList;
        }
        model.addAttribute("page",1);
        model.addAttribute("testList",showList);
        return "student/studentMain";
    }

    @RequestMapping("studentAllByPage")
    public String studentAllByPage(Model model,HttpServletRequest request,@RequestParam Integer page){
        HttpSession session = request.getSession();
        Student student = (Student)session.getAttribute("user");
        model.addAttribute("user",student);
        List<StuPaper> paperList = (List<StuPaper>)session.getAttribute("paperList");

//        判断页数
        int allPage = (paperList.size()%3) == 0 ? paperList.size()/3 : paperList.size()/3 +1;
        page = page <= 0 ? 1 : page;
        List<StuPaper> showList = null;
        if (allPage <= 1){
            showList = paperList;
        }else if (page >= allPage){
            showList = paperList.subList((allPage - 1)*3,paperList.size());
            page = allPage;
        }else {
            showList = paperList.subList((page - 1)*3,page*3);
        }
        model.addAttribute("page",page);
        model.addAttribute("testList",showList);
        return "student/studentMain";
    }

    @RequestMapping("stuTestBeginByPage")
    public String stuTestBegin(Model model,HttpServletRequest request,@RequestParam Integer page){
        HttpSession session = request.getSession();
        SimpleDateFormat sdf =  new SimpleDateFormat( "yyyy-MM-dd HH:mm" );

        Student student = (Student)session.getAttribute("user");
        model.addAttribute("user",student);
        List<StuPaper> paperList = (List<StuPaper>) session.getAttribute("paperList");

        List<StuPaper> result = new ArrayList<>();
        for (StuPaper paper :paperList){
            if (!paper.getStatus().equals("end")){
                result.add(paper);
            }
        }
//        System.out.println(result);
        int allPage = (result.size()%3) == 0 ? result.size()/3 : result.size()/3 +1;
        page = page <= 0 ? 1 : page;
        List<StuPaper> showList = null;
        if (allPage <= 1){
            showList = result;
        }else if (page >= allPage){
            showList = result.subList((allPage - 1)*3,result.size());
            page = allPage;
        }else {
            showList = result.subList((page - 1)*3,page*3);
        }
        model.addAttribute("page",page);
        model.addAttribute("testList",showList);
        return "student/studentMain";
    }

    @RequestMapping("stuTestEndByPage")
    public String stuTestEnd(Model model,HttpServletRequest request,@RequestParam Integer page){
        HttpSession session = request.getSession();
        SimpleDateFormat sdf =  new SimpleDateFormat( "yyyy-MM-dd HH:mm" );

        Student student = (Student)session.getAttribute("user");
        model.addAttribute("user",student);
        List<StuPaper> paperList = (List<StuPaper>) session.getAttribute("paperList");
        List<StuPaper> result = new ArrayList<>();
        for (StuPaper paper :paperList){
            if (paper.getStatus().equals("end")){
                result.add(paper);
            }
        }
        int allPage = (result.size()%3) == 0 ? result.size()/3 : result.size()/3 +1;
        page = page <= 0 ? 1 : page;
        List<StuPaper> showList = null;
        if (allPage <= 1){
            showList = result;
        }else if (page >= allPage){
            showList = result.subList((allPage - 1)*3,result.size());
            page = allPage;
        }else {
            showList = result.subList((page - 1)*3,page*3);
        }
        model.addAttribute("page",page);
        model.addAttribute("testList",showList);
        return "student/studentMain";
    }

    @RequestMapping("seachTest")
    public String seachTest(Model model,HttpServletRequest request,@RequestParam String keyword){
        HttpSession session = request.getSession();

        Student student = (Student)session.getAttribute("user");
        model.addAttribute("user",student);
        List<StuPaper> paperList = (List<StuPaper>) session.getAttribute("paperList");
        List<StuPaper> showList = new ArrayList<>();
        if (paperList.size() > 3){
            showList = paperList.subList(0,3);
        }else {
            showList = paperList;
        }
        List<StuPaper> result = new ArrayList<>();
        for (StuPaper paper :showList){
            if (paper.getTitle().contains(keyword)){
                result.add(paper);
            }
        }
        model.addAttribute("page",1);
        model.addAttribute("testList",result);
        return "student/studentMain";
    }

    @RequestMapping("paperInfo")
    public String paperInfo(Model model,HttpServletRequest request,@RequestParam String paperId){
        HttpSession session = request.getSession();
        TestPaper testPaper = paperService.getById(Integer.parseInt(paperId));
        Student student = (Student)session.getAttribute("user");
        model.addAttribute("stuName",student.getName());
        model.addAttribute("stuClass",student.getGrade()+student.getMajor()+student.getStuClass()+ "班");
        SimpleDateFormat sdf =  new SimpleDateFormat( "yyyy-MM-dd HH:mm" );
        model.addAttribute("testInfo",testPaper.getTitle()+ " (" +sdf.format(testPaper.getStartTime())+ " 到 " +sdf.format(testPaper.getEndTime())+ ")");
        model.addAttribute("teacher",testPaper.getTeacher());
        model.addAttribute("score",testPaper.getStudentScore());

        model.addAttribute("subTime",sdf.format(testPaper.getSubmitTime() == null ? testPaper.getEndTime() : testPaper.getSubmitTime()));

        session.setAttribute("currentPaper",testPaper);   //暂存session

//        获取所有该试卷试题
        List<TestPaperInfoQuestion> infoQuestionList = paperService.getQuestionInfoByPaperId(testPaper.getId());
        model.addAttribute("questionInfos",infoQuestionList);
        return "/student/paperInfo";
    }

    @RequestMapping("doTest")
    public String doTest(Model model,HttpServletRequest request,@RequestParam String paperId){
        HttpSession session = request.getSession();
        TestPaper testPaper = paperService.getById(Integer.parseInt(paperId));
        Student student = (Student)session.getAttribute("user");
        List<Question> questionList = paperService.getQuestionsByPaperId(testPaper.getId());
        List<PaperQuestion> paperQuestionList = paperService.listPaperQuestionByPaperId(testPaper.getId());
        System.out.println(paperQuestionList);
        List<TestQuestion> testQuestionList = new ArrayList<>();
        for (int i = 0;i < paperQuestionList.size();i ++){
            PaperQuestion paperQuestion = paperQuestionList.get(i);
            Question question = questionList.get(i);
            TestQuestion testQuestion = new TestQuestion();
            testQuestion.setPaperQuestionId(paperQuestion.getId());
            testQuestion.setAnswer(question.getAnswer());
            testQuestion.setCatagory(question.getCatagory());
            testQuestion.setDifficulty(question.getDifficulty());
            testQuestion.setOptionA(question.getOptionA());
            testQuestion.setOptionB(question.getOptionB());
            testQuestion.setOptionC(question.getOptionC());
            testQuestion.setOptionD(question.getOptionD());
            testQuestion.setSubject(question.getSubject());
            testQuestion.setTitle(question.getTitle());
            testQuestionList.add(testQuestion);
        }
        model.addAttribute("student",student);
        model.addAttribute("paper",testPaper);
        SimpleDateFormat sdf =  new SimpleDateFormat( "yyyy-MM-dd HH:mm" );
        model.addAttribute("startTime",sdf.format(testPaper.getStartTime()));
        model.addAttribute("endTime",sdf.format(testPaper.getEndTime()));
        model.addAttribute("questions",testQuestionList);
        model.addAttribute("questionNum",questionList.size());
        return "/student/testPaper";
    }

    @RequestMapping("PaperSubmit")
    @ResponseBody
    public JSONObject PaperSubmit(HttpServletRequest request,@RequestBody Map<String,Object> param){
        Integer paperId = (Integer) param.get("paperId");
        List<Object> answerList = (List<Object>) param.get("questionAnswer");
        System.out.println(paperId);
        System.out.println(answerList);
        JSONObject result = new JSONObject();
        result.put("message","yes");
        try {
            paperService.submitScore(paperId,answerList);
        }catch (Exception e){
            e.printStackTrace();
            result.put("message","提交失败：\n" +e.getMessage());
        }
        return result;
    }

    @RequestMapping("paperStarting")
    @ResponseBody
    public Map<String,Object> paperStarting(@RequestParam String paperId,HttpServletRequest request){
        TestPaper testPaper = paperService.getById(Integer.parseInt(paperId));
        Map<String,Object> result = new HashMap<>();
        if (testPaper.getStartTime().compareTo(new Date()) > 0){
            result.put("started","no");
        }else {
            result.put("id",testPaper.getId());
        }
        return result;
    }


    @RequestMapping("studentPwdChange")
    @ResponseBody
    public JSONObject changePwd(@RequestBody Map<String,String> jsonMap,HttpServletRequest request){
        Student student = (Student) request.getSession().getAttribute("user");
        String password = jsonMap.get("oPwd");

        InfoChangeStatusEnum statusEnum = InfoChangeStatusEnum.CHANGESECCESS;

        if (student == null){
           statusEnum = InfoChangeStatusEnum.MATCHFAIL;
        }else if (!password.equals(student.getPassword())){
            statusEnum = InfoChangeStatusEnum.MATCHFAIL;
        }else {
            try {
                studentService.updateStudentPassword(jsonMap.get("nPwd"),student.getUserName());
            }catch (Exception e){
                e.printStackTrace();
                statusEnum = InfoChangeStatusEnum.CHANGEFAIL;
            }
        }

        JSONObject resultJson = new JSONObject();
        resultJson.put("status",statusEnum.toString());
        return  resultJson;
    }
}
