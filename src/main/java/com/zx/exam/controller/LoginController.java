package com.zx.exam.controller;


import com.alibaba.fastjson.JSONObject;
import com.zx.exam.common.LoginStatusEnum;
import com.zx.exam.entity.Administrator;
import com.zx.exam.entity.Student;
import com.zx.exam.entity.Teacher;
import com.zx.exam.service.AdministratorService;
import com.zx.exam.service.StudentService;
import com.zx.exam.service.TeacherService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

/**
 * @ClassName LoginController
 * @Author Sunny
 * @Date 2021年04月11日
 * @Package com.zx.exam.controller
 * @Description 登录
 */
@Controller
public class LoginController {


    @Autowired
    StudentService studentService;

    @Autowired
    TeacherService teacherService;

    @Autowired
    AdministratorService administratorService;


    @PostMapping("/login.do")
    @ResponseBody
    public JSONObject login(@RequestParam String username, @RequestParam String password, HttpServletRequest request) {
        HttpSession session = request.getSession();
        String path = null;
        LoginStatusEnum statusEnum= null;

//        判断登陆者
        Student student = studentService.getByUserName(username);
        if (student != null){
            if (student.getStatus().equals("no")){
                statusEnum = LoginStatusEnum.FORBIDEN;
            }else {
                if (password.equals(student.getPassword())){
                    session.setAttribute("user",student);
                    statusEnum = LoginStatusEnum.PWDCORRECT;
                    path = "student/studentIndex";
                }else {
                    statusEnum = LoginStatusEnum.PWDWRONG;
                }
            }
        }

        Teacher teacher = teacherService.getByUserName(username);
        if (teacher != null){
            if (teacher.getStatus().equals("no")){
                statusEnum = LoginStatusEnum.FORBIDEN;
            }else {
                if (password.equals(teacher.getPassword())) {
                    session.setAttribute("user",teacher);
                    path = "teacher/teacherIndex";
                    statusEnum = LoginStatusEnum.PWDCORRECT;
                }else {
                    statusEnum = LoginStatusEnum.PWDWRONG;
                }
            }
        }

        Administrator admin = administratorService.getByUserName(username);
        if (admin != null){
            if (password.equals(admin.getPassword())) {
                session.setAttribute("user",admin);
                path = "adminIndex";
                statusEnum = LoginStatusEnum.PWDCORRECT;
            }else {
                statusEnum = LoginStatusEnum.PWDWRONG;
            }
        }

        JSONObject object = new JSONObject();
        object.put("status",statusEnum);
        object.put("path",path);

        return object;
    }

}
