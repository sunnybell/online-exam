package com.zx.exam.controller;


import com.alibaba.excel.EasyExcel;
import com.alibaba.fastjson.JSONObject;
import com.zx.exam.entity.Question;
import com.zx.exam.entity.Student;
import com.zx.exam.entity.Teacher;
import com.zx.exam.entity.TestPaper;
import com.zx.exam.entity.excel.ExcelStuScore;
import com.zx.exam.entity.select.DeclaredPaper;
import com.zx.exam.entity.select.StudentScore;
import com.zx.exam.entity.select.TestPaperInfoQuestion;
import com.zx.exam.service.StudentService;
import com.zx.exam.service.TestPaperService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.net.URLEncoder;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * @ClassName TestPaperController
 * @Author Sunny
 * @Date 2021年05月08日
 * @Package com.zx.exam.controller
 * @Description TODO
 */
@Controller
public class TestPaperController {
    @Autowired
    TestPaperService testPaperService;

    @Autowired
    StudentService studentService;

    @RequestMapping("/testPaperInfo")
    public String paperInfo(){
        return "testPaperInfo";
    }

    @RequestMapping("/testPaper")
    public String testPaper(){
        return "testPaper";
    }

    @RequestMapping("/testPaperSample")
    public String testPaper(@RequestParam Integer paperId, Model model){
        System.out.println(paperId);
        TestPaper paper = testPaperService.getById(paperId);
        model.addAttribute("paper",paper);
        SimpleDateFormat sdf =  new SimpleDateFormat( "yyyy-MM-dd HH:mm" );
        model.addAttribute("startTime",sdf.format(paper.getStartTime()));
        model.addAttribute("endTime",sdf.format(paper.getEndTime()));
        List<Question> questionList = testPaperService.getQuestionsByPaperId(paperId);
        model.addAttribute("questions",questionList);
        model.addAttribute("questionNum",questionList.size());
//        System.out.println(paper);
        return "teacher/paperPreview";
    }

    @RequestMapping("/searchTest")
    @ResponseBody
    public List<DeclaredPaper> searchTest(@RequestParam String keyword, HttpServletRequest request){
        List<DeclaredPaper> papers = (List<DeclaredPaper>) request.getSession().getAttribute("papers");
        List<DeclaredPaper> result = new ArrayList<>();
        for (DeclaredPaper paper : papers){
            if (paper.getTitle().contains(keyword)){
                result.add(paper);
            }
        }
        System.out.println(result);
        return result;
    }

    @RequestMapping("/screenTest")
    @ResponseBody
    public List<DeclaredPaper> screenTest(@RequestParam String time,@RequestParam String subject,HttpServletRequest request) throws ParseException {
        List<DeclaredPaper> papers = (List<DeclaredPaper>) request.getSession().getAttribute("papers");
        List<DeclaredPaper> result = new ArrayList<>();
        SimpleDateFormat format = new SimpleDateFormat("yyyy年MM月dd日");
        for (DeclaredPaper p : papers){
            if (subject.equals("全部") || subject.equals(p.getSubject())){
                Date date = format.parse(p.getStart());
                if (time.equals("全部")){
                    result.add(p);
                }else if (time.equals("未开始")){
                    if (date.compareTo(new Date()) > 0){
                        result.add(p);
                    }
                }else {
                    if (date.compareTo(new Date()) <= 0){
                        result.add(p);
                    }
                }
            }
        }
        return result;
    }

    @RequestMapping("/deletePaper")
    @ResponseBody
    public List<DeclaredPaper> deletePaper(@RequestParam String paperName,HttpServletRequest request){
        testPaperService.deleteTestByName(paperName);
        Teacher teacher = (Teacher) request.getSession().getAttribute("user");
        return  testPaperService.listPaper(teacher.getName());
    }

    @RequestMapping("/studentScore")
    @ResponseBody
    public List<StudentScore> studentScore(@RequestParam String paperName, HttpServletRequest request){
        List<DeclaredPaper> papers = (List<DeclaredPaper>) request.getSession().getAttribute("papers");
        List<StudentScore> studentScoreList = testPaperService.listPaperScoreByName(paperName);
        request.getSession().setAttribute("paperStudentsScore",studentScoreList);
        return studentScoreList;
    }

    @RequestMapping("/searchTestStudent")
    @ResponseBody
    public List<StudentScore> searchTestStudent(@RequestParam String keyword,HttpServletRequest request){
        HttpSession session = request.getSession();
        List<StudentScore> studentScores = (List<StudentScore>) session.getAttribute("paperStudentsScore");
        List<StudentScore> targetList = new ArrayList<>();
        for (StudentScore studentScore : studentScores){
            if (studentScore.getName().contains(keyword)){
                targetList.add(studentScore);
            }
        }
        return targetList;
    }

    /**
     * @param stuUsername
     * @param paperName
     * @param model
     * @param request
     * @return
     */
    @RequestMapping("/studentScorePaper")
    public String studentScorePaper(@RequestParam String stuUsername,@RequestParam String paperName,Model model,HttpServletRequest request){
        HttpSession session = request.getSession();
        Student student = studentService.getByUserName(stuUsername);
        model.addAttribute("stuName",student.getName());
        model.addAttribute("stuClass",student.getGrade()+student.getMajor()+student.getStuClass()+ "班");
        SimpleDateFormat sdf =  new SimpleDateFormat( "yyyy-MM-dd HH:mm" );
        TestPaper testPaper = testPaperService.getByTitleAndStuId(paperName,student.getId());
        model.addAttribute("testInfo",testPaper.getTitle()+ " (" +sdf.format(testPaper.getStartTime())+ " 到 " +sdf.format(testPaper.getEndTime())+ ")");
        model.addAttribute("teacher",testPaper.getTeacher());
        model.addAttribute("score",testPaper.getStudentScore());
        try {
            model.addAttribute("subTime",sdf.format(testPaper.getSubmitTime()));
        }catch (Exception e){
            e.printStackTrace();
            model.addAttribute("subTime","");
        }
        session.setAttribute("currentPaper",testPaper);   //暂存session

        /*获取所有该试卷试题*/
        List<TestPaperInfoQuestion> infoQuestionList = testPaperService.getQuestionInfoByPaperId(testPaper.getId());
        model.addAttribute("questionInfos",infoQuestionList);
        return "testPaperInfo";
    }

    /**
     * @param paramMap
     * @param request
     * @return
     */
    @RequestMapping("/markPaper")
    @ResponseBody
    public JSONObject markPaper(@RequestBody Map<String,Object> paramMap, HttpServletRequest request){
        JSONObject result = new JSONObject();
        List<Object> scores = (List<Object>) paramMap.get("shortArr");

        TestPaper paper = (TestPaper) request.getSession().getAttribute("currentPaper");
        testPaperService.markPaper(scores, paramMap.get("sum").toString(),paper.getId());

        return result;
    }

    /**
     * @param paperTitle
     * @param response
     * @throws IOException
     * 导出学生成绩
     */
    @RequestMapping("/exportScore")
    public void exportScore(@RequestParam String paperTitle, HttpServletResponse response) throws IOException {
        List<ExcelStuScore> list = testPaperService.exportScoreByPaper(paperTitle);
        response.setContentType("application/vnd.ms-excel");
        response.setCharacterEncoding("utf-8");
        // 这里URLEncoder.encode可以防止中文乱码,和easyexcel没有关系
        String fileName = URLEncoder.encode(paperTitle, "UTF-8");
        response.setHeader("Content-disposition", "attachment;filename=" + fileName + ".xlsx");
        EasyExcel.write(response.getOutputStream(), ExcelStuScore.class).sheet().doWrite(list);
    }
}
