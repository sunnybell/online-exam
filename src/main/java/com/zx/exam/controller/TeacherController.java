package com.zx.exam.controller;


import com.alibaba.fastjson.JSONObject;
import com.zx.exam.common.InfoChangeStatusEnum;
import com.zx.exam.entity.Teacher;
import com.zx.exam.entity.select.DeclaredPaper;
import com.zx.exam.entity.select.StudentInfo;
import com.zx.exam.service.QuestionService;
import com.zx.exam.service.StudentService;
import com.zx.exam.service.TeacherService;
import com.zx.exam.service.TestPaperService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * @ClassName TeacherController
 * @Author Sunny
 * @Date 2021年04月18日
 * @Package com.zx.exam.controller
 * @Description TODO
 */
@Controller
@RequestMapping("teacher")
public class TeacherController {

    @Autowired
    TeacherService teacherService;

    @Autowired
    QuestionService questionService;

    @Autowired
    StudentService studentService;

    @Autowired
    TestPaperService testPaperService;

    @RequestMapping("teacherIndex")
    public String teacherIndex(Model model, HttpServletRequest request){
        HttpSession session = request.getSession();

        model.addAttribute("subjects",null);
        model.addAttribute("user",session.getAttribute("user"));
        Date date = new Date();
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy年MM月dd日");
        model.addAttribute("date",dateFormat.format(date));

        /*添加初始试卷数据*/
        String teacher = ((Teacher)session.getAttribute("user")).getName();
        List<DeclaredPaper> declaredPaperList = testPaperService.listPaper(teacher);
        model.addAttribute("papers",declaredPaperList);
        session.setAttribute("papers",declaredPaperList);

        /*添加初始试题数据*/
        model.addAttribute("questionList", questionService.getAllQuestion());

        StudentInfo stuInfo = studentService.getStudentsInfo();
        model.addAttribute("majors",stuInfo.getMajors());
        model.addAttribute("grades",stuInfo.getGrades());
        model.addAttribute("classes",stuInfo.getClasses());
        /*已有科目*/
        model.addAttribute("subjects",questionService.getAllSubjects());
        return "teacher/teacherMain";
    }



    @RequestMapping("teacherPwdChange")
    @ResponseBody
    public JSONObject pwdChange(@RequestBody Map<String,String> teacherUpdateMap){
        Teacher teacher = teacherService.getByUserName(teacherUpdateMap.get("username"));
        String pwd = teacherUpdateMap.get("oPwd");

        InfoChangeStatusEnum statusEnum = InfoChangeStatusEnum.CHANGESECCESS;

        if (teacher == null){
            statusEnum = InfoChangeStatusEnum.MATCHFAIL;
        }else if (!pwd.equals(teacher.getPassword())){
            statusEnum = InfoChangeStatusEnum.MATCHFAIL;
        }else {
            try {
                teacherService.updateTeacherPassword(teacherUpdateMap.get("nPwd"),teacher.getUserName());
            }catch (Exception e){
                e.printStackTrace();
                statusEnum = InfoChangeStatusEnum.CHANGEFAIL;
            }
        }

        JSONObject resulJson = new JSONObject();
        resulJson.put("status",statusEnum.toString());
        return resulJson;
    }





}
