package com.zx.exam.controller;


import com.alibaba.fastjson.JSONObject;
import com.zx.exam.entity.Logger;
import com.zx.exam.entity.Teacher;
import com.zx.exam.entity.requestbody.DeclareNewPaperRequestBody;
import com.zx.exam.entity.select.StudentInfo;
import com.zx.exam.service.LogService;
import com.zx.exam.service.StudentService;
import com.zx.exam.service.TestPaperService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.Date;
import java.util.List;

/**
 * @ClassName NewTestController
 * @Author Sunny
 * @Date 2021年05月12日
 * @Package com.zx.exam.controller
 * @Description TODO
 */
@Controller
public class NewTestController {

    @Autowired
    StudentService studentService;
    @Autowired
    TestPaperService paperService;
    @Autowired
    LogService logService;

    static Teacher teacher = null;

    @RequestMapping("testStudentNumber")
    @ResponseBody
    public JSONObject testStudentNumber(@RequestBody StudentInfo info, HttpServletRequest request){
        HttpSession session = request.getSession();
        teacher = (Teacher)request.getSession().getAttribute("user");
        List<Integer> stuIds = studentService.getStudentIdWithInfo(info);
        session.setAttribute("selectedStuId",stuIds);
        System.out.println(stuIds);
        JSONObject result = new JSONObject();
        result.put("num",stuIds.size());
        return result;
    }

    @RequestMapping("declareNewPaper")
    @ResponseBody
    public JSONObject declareNewPaper(@RequestBody DeclareNewPaperRequestBody requestBody,HttpServletRequest request){
        HttpSession session = request.getSession();
        teacher = (Teacher)session.getAttribute("user");
        JSONObject result = new JSONObject();
        result.put("message","发布成功！");
        result.put("success","yes");
        try {
            paperService.declarePaperByParam(requestBody,session);
            result.put("sampleTest",paperService.getDeclaredPaper());
            logService.addLog(new Logger(0,teacher.getName(),"发布考试",new Date()));
        }catch (Exception e){
            e.printStackTrace();
            result.put("success","no");
            result.put("message","发布失败！\n" +e.getMessage());
        }
        return result;
    }
}
