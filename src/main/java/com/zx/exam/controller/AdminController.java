package com.zx.exam.controller;


import com.alibaba.fastjson.JSONObject;
import com.zx.exam.common.InfoChangeStatusEnum;
import com.zx.exam.entity.Administrator;
import com.zx.exam.entity.Logger;
import com.zx.exam.entity.Student;
import com.zx.exam.entity.Teacher;
import com.zx.exam.entity.select.StudentInfo;
import com.zx.exam.service.AdministratorService;
import com.zx.exam.service.LogService;
import com.zx.exam.service.StudentService;
import com.zx.exam.service.TeacherService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * @ClassName AdminController
 * @Author Sunny
 * @Date 2021年05月04日
 * @Package com.zx.exam.controller
 * @Description TODO
 */
@Controller
public class AdminController {
    @Autowired
    AdministratorService administratorService;

    @Autowired
    StudentService studentService;

    @Autowired
    TeacherService teacherService;

    @Autowired
    LogService logService;

    static Administrator admin = null;

    @RequestMapping("adminIndex")
    public String adminIndex(Model model, HttpServletRequest request){
        admin = (Administrator)request.getSession().getAttribute("user");
        model.addAttribute("user",request.getSession().getAttribute("user"));
        Date date = new Date();
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy年MM月dd日");
        model.addAttribute("date",dateFormat.format(date));

        model.addAttribute("teachers",teacherService.listTeacher());
        model.addAttribute("students",studentService.getStudents());
        StudentInfo info = studentService.getStudentsInfo();
        model.addAttribute("majors",info.getMajors());
        model.addAttribute("grades",info.getGrades());
        model.addAttribute("stuClasses",info.getClasses());
        model.addAttribute("logs",logService.listLog());

        logService.addLog(new Logger(0,admin.getUserName(),"登录系统",new Date()));
        return "admin/adminMain";
    }

    @RequestMapping("adminPwdChange")
    @ResponseBody
    public JSONObject pwdChange(@RequestBody Map<String,String> adminUpdateMap,HttpServletRequest request){
        HttpSession session = request.getSession();
        Administrator administrator = (Administrator) session.getAttribute("user");
        String pwd = adminUpdateMap.get("oPwd");

        InfoChangeStatusEnum statusEnum = InfoChangeStatusEnum.CHANGESECCESS;

        if (!administrator.getUserName().equals(adminUpdateMap.get("username"))){
            statusEnum = InfoChangeStatusEnum.MATCHFAIL;
        }else if (!pwd.equals(administrator.getPassword())){
            statusEnum = InfoChangeStatusEnum.MATCHFAIL;
        }else {
            try {
                administratorService.updatePassword(adminUpdateMap.get("nPwd"),administrator.getUserName());
                logService.addLog(new Logger(0,admin.getUserName(),"修改密码",new Date()));
            }catch (Exception e){
                e.printStackTrace();
                statusEnum = InfoChangeStatusEnum.CHANGEFAIL;
            }
        }

        JSONObject resulJson = new JSONObject();
        resulJson.put("status",statusEnum.toString());
        return resulJson;
    }

    @RequestMapping("newStudent")
    @ResponseBody
    public JSONObject newStudent(@RequestBody Student student){
        JSONObject result = new JSONObject();
        result.put("message","新建成功");
        try {
            studentService.addStudent(student);
            StudentInfo studentInfo = studentService.getStudentsInfo();
            result.put("students",studentService.getStudents());
            result.put("majors",studentInfo.getMajors());
            result.put("grades",studentInfo.getGrades());
            result.put("stuClasses",studentInfo.getClasses());
            logService.addLog(new Logger(0,admin.getUserName(),"新增学生",new Date()));
        } catch (Exception e) {
            result.put("message","新建失败:/n" +e.getMessage());
        }
        return result;

    }

    @RequestMapping("deleteStudentById")
    @ResponseBody
    public JSONObject deleteStudentById(@RequestParam Integer id){
        studentService.deleteStudentById(id);
        StudentInfo studentInfo = studentService.getStudentsInfo();
        JSONObject result = new JSONObject();
        result.put("students",studentService.getStudents());
        result.put("majors",studentInfo.getMajors());
        result.put("grades",studentInfo.getGrades());
        result.put("stuClasses",studentInfo.getClasses());
        logService.addLog(new Logger(0,admin.getUserName(),"删除学生",new Date()));
        return result;
    }

    @RequestMapping("importStudents")
    @ResponseBody
    public JSONObject importStudents(@RequestParam MultipartFile file) {
        JSONObject resultJson = new JSONObject();
        resultJson.put("message","导入成功");
        try {
            studentService.importStudentsWithStream(file.getInputStream());
            StudentInfo studentInfo = studentService.getStudentsInfo();
            resultJson.put("students",studentService.getStudents());
            resultJson.put("majors",studentInfo.getMajors());
            resultJson.put("grades",studentInfo.getGrades());
            resultJson.put("classes",studentInfo.getClasses());
            logService.addLog(new Logger(0,admin.getUserName(),"导入学生",new Date()));
        } catch (Exception e) {
            resultJson.put("message","导入失败:/n" +e.getMessage());
        }
        return resultJson;
    }

    @RequestMapping("getStudentById")
    @ResponseBody
    public Student updateStudentById(@RequestParam Integer id,HttpServletRequest request) {
        Student student = studentService.getStudentById(id);
        request.getSession().setAttribute("student",student);
        return student;
    }

    @RequestMapping("updateStudent")
    @ResponseBody
    public JSONObject updateStudent(@RequestBody Student student,HttpServletRequest request){
        Student current = (Student) request.getSession().getAttribute("student");
        student.setId(current.getId());
        JSONObject resultJson = new JSONObject();
        resultJson.put("message","修改成功");
        try {
            studentService.updateStudent(student);
            StudentInfo studentInfo = studentService.getStudentsInfo();
            resultJson.put("students",studentService.getStudents());
            resultJson.put("majors",studentInfo.getMajors());
            resultJson.put("grades",studentInfo.getGrades());
            resultJson.put("classes",studentInfo.getClasses());
            logService.addLog(new Logger(0,admin.getUserName(),"修改学生信息",new Date()));
        } catch (Exception e) {
            resultJson.put("message","修改失败:/n" +e.getMessage());
        }
        return resultJson;
    }

    @RequestMapping("forbidStudent")
    @ResponseBody
    public Student forbidStudent(@RequestParam Integer id){
        studentService.forbidStudent(id);
        Student student = studentService.getStudentById(id);
        logService.addLog(new Logger(0,admin.getUserName(),"禁用学生",new Date()));
        return student;
    }

    @RequestMapping("releaseStudent")
    @ResponseBody
    public Student releaseStudent(@RequestParam Integer id){
        studentService.releaseStudent(id);
        Student student = studentService.getStudentById(id);
        logService.addLog(new Logger(0,admin.getUserName(),"解禁学生",new Date()));
        return student;
    }

    @RequestMapping("searchStudent")
    @ResponseBody
    public List<Student> searchStudent(@RequestParam String keyword){
        logService.addLog(new Logger(0,admin.getUserName(),"查询学生",new Date()));
        return studentService.getStudentsByKeyword(keyword);
    }

    @RequestMapping("screenStudent")
    @ResponseBody
    public List<Student> screenStudent(@RequestParam String major,@RequestParam String grade,@RequestParam String stuClass){
        logService.addLog(new Logger(0,admin.getUserName(),"筛选学生",new Date()));
        return studentService.getStudentsByParam(major,grade,stuClass);
    }

    @RequestMapping("newTeacher")
    @ResponseBody
    public JSONObject newTeacher(@RequestBody Teacher teacher){
        JSONObject result = new JSONObject();
        result.put("message","新建成功");
        try {
            teacherService.addTeacher(teacher);
            result.put("teachers",teacherService.listTeacher());
            logService.addLog(new Logger(0,admin.getUserName(),"新增教师",new Date()));
        } catch (Exception e) {
            result.put("message","新建失败:/n" +e.getMessage());
        }
        return result;
    }

    @RequestMapping("deleteTeacherById")
    @ResponseBody
    public JSONObject deleteTeacherById(@RequestParam Integer id){
        JSONObject result = new JSONObject();
        result.put("message","删除成功");
        try {
            teacherService.deleteById(id);
            result.put("teachers",teacherService.listTeacher());
            logService.addLog(new Logger(0,admin.getUserName(),"删除教师",new Date()));
        } catch (Exception e) {
            result.put("message","删除失败:/n" +e.getMessage());
        }
        return result;
    }

    @RequestMapping("getTeacherById")
    @ResponseBody
    public Teacher getTeacherById(@RequestParam Integer id,HttpServletRequest request) {
        Teacher teacher = teacherService.getById(id);
        request.getSession().setAttribute("teacher",teacher);
        return teacher;
    }

    @RequestMapping("updateTeacher")
    @ResponseBody
    public JSONObject updateTeacher(@RequestBody Teacher teacher,HttpServletRequest request){
        Teacher current = (Teacher) request.getSession().getAttribute("teacher");
        teacher.setId(current.getId());
        JSONObject resultJson = new JSONObject();
        resultJson.put("message","修改成功");
        try {
            teacherService.updateTeacher(teacher);
            resultJson.put("teachers",teacherService.listTeacher());
            logService.addLog(new Logger(0,admin.getUserName(),"更改教师",new Date()));
        } catch (Exception e) {
            resultJson.put("message","修改失败:/n" +e.getMessage());
        }
        return resultJson;
    }

    @RequestMapping("forbidTeacher")
    @ResponseBody
    public Teacher forbidTeacher(@RequestParam Integer id){
        logService.addLog(new Logger(0,admin.getUserName(),"禁用教师",new Date()));
        teacherService.forbidTeacher(id);
        return teacherService.getById(id);
    }

    @RequestMapping("releaseTeacher")
    @ResponseBody
    public Teacher releaseTeacher(@RequestParam Integer id){
        logService.addLog(new Logger(0,admin.getUserName(),"解禁教师",new Date()));
        teacherService.releaseTeacher(id);
        return teacherService.getById(id);
    }

    @RequestMapping("searchTeacher")
    @ResponseBody
    public List<Teacher> searchTeacher(@RequestParam String keyword){
        List<Teacher> teachers = null;
        if (keyword.equals("")){
            teachers = teacherService.listTeacher();
        }else {
            teachers = teacherService.getTeachersByKeyword(keyword);
        }
        logService.addLog(new Logger(0,admin.getUserName(),"查询教师",new Date()));
        return teachers;
    }

    @RequestMapping("deleteLoggerById")
    @ResponseBody
    public JSONObject deleteLoggerById(@RequestParam Integer id,HttpServletRequest request){
        JSONObject result = new JSONObject();
        result.put("message","删除成功");
        try {
            logService.deleteLog(id);
            result.put("logs",logService.listLog());
            logService.addLog(new Logger(0,admin.getUserName(),"删除日志",new Date()));
        } catch (Exception e) {
            result.put("message","删除失败:/n" +e.getMessage());
        }
        return result;
    }

    @RequestMapping("searchLogs")
    @ResponseBody
    public List<Logger> searchLogs(@RequestParam String key,@RequestParam String param){
        List<Logger> loggerList = logService.listLog();
        List<Logger> result = new ArrayList<>();
        for (Logger logger : loggerList){
            if (logger.getOperator().contains(key) || "".equals(key)){
                long hours =  dateSubStract(logger.getTime());
                switch (param){
                    case "1周内" :
                        if (hours < (24*7)) {
                            result.add(logger);
                        }
                        break;
                    case "1小时内":
                        if (hours <= 1) {
                            result.add(logger);
                        }
                        break;
                    case "1天内":
                        if (hours < 24) {
                            result.add(logger);
                        }
                        break;
                    default:
                        result.add(logger);
                        break;
                }
            }
        }
        logService.addLog(new Logger(0,admin.getUserName(),"查询日志",new Date()));
        return result;
    }

    private long dateSubStract(Date compareDate){
        long nh = 1000*60*60;
        long hours = (new Date().getTime() - compareDate.getTime())/nh;
        return hours;
    }
}
