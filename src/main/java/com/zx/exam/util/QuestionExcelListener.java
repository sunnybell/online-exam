package com.zx.exam.util;


import com.alibaba.excel.context.AnalysisContext;
import com.alibaba.excel.event.AnalysisEventListener;
import com.zx.exam.entity.excel.ExcelQuestionModel;
import com.zx.exam.entity.requestbody.CreateQuestion;
import com.zx.exam.entity.requestbody.Option;

import java.util.ArrayList;
import java.util.List;

/**
 * @ClassName ExcelListener
 * @Author Sunny
 * @Date 2021年04月23日
 * @Package com.zx.exam.util
 * @Description TODO
 */
public class QuestionExcelListener extends AnalysisEventListener<ExcelQuestionModel> {
    //如果有需要将读取到的数据插入数据库的话，默认是每5条插入数据库
    private static  final int BATCH_SIZE =5;

    List<CreateQuestion> list = new ArrayList<>();

    /**
     * 数据读取的核心方法，读取数据会执行这个方法
     * ExcelEntity：表格对应的实体
     * AnalysisContext：分析上下文
     * @param excelEntity
     * @param analysisContext
     */
    @Override
    public void invoke(ExcelQuestionModel excelEntity, AnalysisContext analysisContext) {
        //将读取到的数据存入数据库

        CreateQuestion question = new CreateQuestion();
        question.setTitle(excelEntity.getTitle());
        question.setSubject(excelEntity.getSubject());
        question.setCatagory(excelEntity.getCatagory());
        question.setDifficulty(excelEntity.getDifficulty());
        question.setAnswer(excelEntity.getAnswer());

//        添加四个选项
        List<Option> optionList = new ArrayList<>();
        Option optionA = new Option();
        optionA.setName("A");
        optionA.setContent(excelEntity.getOptionA());
        optionList.add(optionA);

        Option optionB = new Option();
        optionB.setName("B");
        optionB.setContent(excelEntity.getOptionB());
        optionList.add(optionB);

        Option optionC = new Option();
        optionC.setName("C");
        optionC.setContent(excelEntity.getOptionC());
        optionList.add(optionC);

        Option optionD = new Option();
        optionD.setName("D");
        optionD.setContent(excelEntity.getOptionD());
        optionList.add(optionD);

        question.setOptions(optionList);
        list.add(question);
    }

    @Override
    public void doAfterAllAnalysed(AnalysisContext analysisContext) {

    }

    public List<CreateQuestion> getList() {
        return list;
    }



}
