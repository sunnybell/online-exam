package com.zx.exam.util;


import com.alibaba.excel.context.AnalysisContext;
import com.alibaba.excel.event.AnalysisEventListener;
import com.zx.exam.entity.Student;
import com.zx.exam.entity.excel.ExcelStudent;

import java.util.ArrayList;
import java.util.List;

/**
 * @ClassName StudentExcelListener
 * @Author Sunny
 * @Date 2021年05月20日
 * @Package com.zx.exam.util
 * @Description TODO
 */
public class StudentExcelListener extends AnalysisEventListener<ExcelStudent> {
    //如果有需要将读取到的数据插入数据库的话，默认是每5条插入数据库
    private static  final int BATCH_SIZE =5;

    List<Student> list = new ArrayList<>();

    @Override
    public void invoke(ExcelStudent excelStudent, AnalysisContext analysisContext) {
        Student student = new Student();
        student.setUserName(excelStudent.getUsername());
        student.setPassword(excelStudent.getPassword());
        student.setMajor(excelStudent.getMajor());
        student.setGrade(excelStudent.getGrade());
        student.setStuClass(excelStudent.getStuClass());
        student.setName(excelStudent.getName());
        list.add(student);
    }

    @Override
    public void doAfterAllAnalysed(AnalysisContext analysisContext) {

    }

    public List<Student> getList() {
        return list;
    }
}
