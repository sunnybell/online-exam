package com.zx.exam.config.inceptor;


import com.zx.exam.service.StudentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.HandlerInterceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * @ClassName LoginInceptor
 * @Author Sunny
 * @Date 2021年04月11日
 * @Package com.zx.exam.config.inceptor
 * @Description TODO
 */
@Component
public class LoginInceptor implements HandlerInterceptor {

    @Autowired
    StudentService studentService;



    /**
     * @param request
     * @param response
     * @param handler
     * @return
     * @throws Exception
     * 请求到达时处理
     */
    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
//        获取访问session
        Object user = request.getSession().getAttribute("user");
        System.out.println("拦截了！" +request.getRequestURI());
        System.out.println("session为： " +user);
//        session是否存在
        if (user == null || user.equals("")){
            response.sendRedirect("/");
            return false;
        }
        return true;
    }




}
