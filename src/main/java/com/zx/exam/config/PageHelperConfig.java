package com.zx.exam.config;


import com.github.pagehelper.PageHelper;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.Properties;

/**
 * @ClassName PageHelperConfig
 * @Author Sunny
 * @Date 2021年05月05日
 * @Package com.zx.exam.config
 * @Description TODO
 */
@Configuration
public class PageHelperConfig {

    @Bean
    PageHelper pageHelper(){
        PageHelper pageHelper = new PageHelper();
        Properties p = new Properties();
        p.setProperty("offsetAsPageNum", "true");
        p.setProperty("rowBoundsWithCount", "true");
        p.setProperty("reasonable", "true");
        pageHelper.setProperties(p);
        return pageHelper;
    }
}
