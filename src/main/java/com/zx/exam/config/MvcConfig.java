package com.zx.exam.config;


import com.zx.exam.config.inceptor.LoginInceptor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

/**
 * @ClassName MvcConfig
 * @Author Sunny
 * @Date 2021年04月11日
 * @Package com.zx.exam.config
 * @Description TODO
 */
@Configuration
public class MvcConfig implements WebMvcConfigurer {

    @Autowired
    LoginInceptor loginInceptor;

    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        registry.addInterceptor(loginInceptor).addPathPatterns("/**").excludePathPatterns("/","/login.do","/static/**","/js/**");

    }

    @Override
    public void addViewControllers(ViewControllerRegistry registry) {
        registry.addViewController("/").setViewName("login");
    }

}
