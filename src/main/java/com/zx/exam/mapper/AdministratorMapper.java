package com.zx.exam.mapper;

import com.zx.exam.entity.Administrator;
import org.apache.ibatis.annotations.Mapper;

/**
 * @ClassName AdministratorMapper
 * @Author Sunny
 * @Date 2021年04月13日
 * @Package com.zx.exam.mapper
 * @Description TODO
 */
@Mapper
public interface AdministratorMapper {
    Administrator getByUserName(String userName);

    void updatePasswordByUsername(String password,String username);
}
