package com.zx.exam.mapper;

import com.zx.exam.entity.Question;
import com.zx.exam.entity.Subject;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * @ClassName QuestionMapper
 * @Author Sunny
 * @Date 2021年04月27日
 * @Package com.zx.exam.mapper
 * @Description TODO
 */
@Mapper
public interface QuestionMapper {
    List<Integer> getQuestionIdByCondition(String catagory,String diff,Integer num);

    /**
     * @return 返回所有科目
     */
    List<String> getAllSubjects();

    Subject getSubjectByName(String name);

    /*新增试题*/
    Integer addQuestion(Question question);

    /*修改试题id*/
    Integer updateQuestionId(Integer nId,Integer oId);

    /*返回所有试题*/
    List<Question> getAllQuestion();
    Question getQuestionById(Integer id);

//    查询搜索的试题
    List<Question> listQuestionByKey(String key);

    /*删除*/
    Integer deleteQuestionById(Integer id);
    Integer deleteOptionsByQuestionId(Integer id);
}
