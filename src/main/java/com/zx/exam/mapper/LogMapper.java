package com.zx.exam.mapper;

import com.zx.exam.entity.Logger;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * @ClassName LogMapper
 * @Author Sunny
 * @Date 2021年04月18日
 * @Package com.zx.exam.mapper
 * @Description TODO
 */
@Mapper
public interface LogMapper {
    int addLog(Logger logger);

    int deleteLog(Integer id);

    Logger selectByOperator(String name);

    List<Logger> listLog();

}
