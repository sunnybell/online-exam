package com.zx.exam.mapper;


import com.zx.exam.entity.Student;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * @ClassName StudentMapper
 * @Author Sunny
 * @Date 2021年04月11日
 * @Package com.zx.exam.mapper
 * @Description TODO
 */
@Mapper
public interface StudentMapper {

    List<Student> getStudents();

    Integer addStudent(Student student);
    void deleteStudentById(Integer id);
    Student getStudentById(Integer id);
    void updateStudent(Student student);
    void forbidStudent(Integer id);
    void releaseStudent(Integer id);

    List<Student> getStudentsByKeyword(String key);

    Student getByUserName(String userName);

    Integer updateStudentPassword(String password,String username);

//    专业
    List<String> getMajors();
//    年级
    List<String> getGrades();
//    班级
    List<String> getClasses();

    List<Integer> getIdByParam(List<String> majors,List<String> grades,List<String> classes);
}
