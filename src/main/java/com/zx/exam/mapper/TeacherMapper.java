package com.zx.exam.mapper;

import com.zx.exam.entity.Teacher;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * @ClassName TeacherMapper
 * @Author Sunny
 * @Date 2021年04月13日
 * @Package com.zx.exam.mapper
 * @Description TODO
 */
@Mapper
public interface TeacherMapper {
    Teacher getByUserName(String userName);

    Integer updateTeacherPassword(String password,String username);

    List<Teacher> listTeacher();

    int addTeacher(Teacher teacher);

    void deleteById(Integer id);

    Teacher getById(Integer id);

    void updateTeacher(Teacher teacher);

    void forbidTeacher(Integer id);

    void releaseTeacher(Integer id);

    List<Teacher> getTeachersByKeyword(String key);
}
