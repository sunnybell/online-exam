package com.zx.exam.mapper;

import com.zx.exam.entity.PaperQuestion;
import com.zx.exam.entity.Question;
import com.zx.exam.entity.TestPaper;
import com.zx.exam.entity.excel.ExcelStuScore;
import com.zx.exam.entity.select.DeclaredPaper;
import com.zx.exam.entity.select.StuPaper;
import com.zx.exam.entity.select.StudentScore;
import com.zx.exam.entity.select.TestPaperInfoQuestion;
import org.apache.ibatis.annotations.Mapper;

import java.util.Date;
import java.util.List;

/**
 * @ClassName TestPaperMapper
 * @Author Sunny
 * @Date 2021年05月12日
 * @Package com.zx.exam.mapper
 * @Description TODO
 */
@Mapper
public interface TestPaperMapper {
    int addPaper(TestPaper paper);

    int addPaperQuestion(PaperQuestion paperQuestion);

    TestPaper getDeclaredPaper();

    TestPaper getById(Integer id);

    List<Question> getQuestionsByPaperId(Integer id);

    String getQuestionNumByPaperId(Integer id);

    List<TestPaper> listPaper();

    List<DeclaredPaper> listPaperByKeyWord(String key);

    List<StudentScore> listPaperScoreByName(String paperName);

    TestPaper getByTitleAndStuId(String title,Integer stuId);

    List<TestPaperInfoQuestion> getQuestionInfoByPaperId(Integer id);

    void updateQuestionScoreById(String score,Integer id);

    void updatePaperScoreById(String score,Integer id);

    List<ExcelStuScore> exportScoreByPaper(String paperTitle);

    void deletePaperQuestionByPaperId(Integer paperId);

    List<TestPaper> listByPaperTitle(String paperName);

    void deletePaperByName(String paperName);

    List<TestPaper> listPaperByStudentId(Integer id);

    void setSubmitScore(Integer id);
    void setSubmitAnswer(String answer,Integer id);
    PaperQuestion getPaperQuestionById(Integer id);

    List<PaperQuestion> listPaperQuestionByPaperId(Integer id);

    void setPaperSubmitTimeById(Integer id, Date submitTime);

}
