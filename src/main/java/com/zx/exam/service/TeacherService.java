package com.zx.exam.service;

import com.zx.exam.entity.Teacher;
import com.zx.exam.mapper.StudentMapper;

import java.util.List;

/**
 * @ClassName TeacherService
 * @Author Sunny
 * @Date 2021年04月13日
 * @Package com.zx.exam.service
 * @Description TODO
 */
public interface TeacherService {
    Teacher getByUserName(String userName);
    Integer updateTeacherPassword(String password,String username);

    int addTeacher(Teacher teacher);
    List<Teacher> listTeacher();

    void deleteById(Integer id);
    Teacher getById(Integer id);

    void updateTeacher(Teacher teacher);

    void forbidTeacher(Integer id);

    void releaseTeacher(Integer id);

    List<Teacher> getTeachersByKeyword(String key);
}
