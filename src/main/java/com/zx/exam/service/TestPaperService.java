package com.zx.exam.service;

import com.zx.exam.entity.PaperQuestion;
import com.zx.exam.entity.Question;
import com.zx.exam.entity.TestPaper;
import com.zx.exam.entity.excel.ExcelStuScore;
import com.zx.exam.entity.requestbody.DeclareNewPaperRequestBody;
import com.zx.exam.entity.select.DeclaredPaper;
import com.zx.exam.entity.select.StuPaper;
import com.zx.exam.entity.select.StudentScore;
import com.zx.exam.entity.select.TestPaperInfoQuestion;

import javax.servlet.http.HttpSession;
import java.text.ParseException;
import java.util.List;

/**
 * @ClassName TestPaperService
 * @Author Sunny
 * @Date 2021年05月12日
 * @Package com.zx.exam.service
 * @Description TODO
 */
public interface TestPaperService {
    void declarePaperByParam(DeclareNewPaperRequestBody requestBody, HttpSession session) throws ParseException;
    TestPaper getDeclaredPaper();

    TestPaper getById(Integer id);

    List<Question> getQuestionsByPaperId(Integer id);

    List<DeclaredPaper> listPaper(String teacher);

    List<StudentScore> listPaperScoreByName(String paperName);

    TestPaper getByTitleAndStuId(String title,Integer stuId);

    List<TestPaperInfoQuestion> getQuestionInfoByPaperId(Integer id);

    void markPaper(List<Object> shortMarkScores, String sum, Integer paperId);

    List<ExcelStuScore> exportScoreByPaper(String paperTitle);

    void deleteTestByName(String paperName);

    List<StuPaper> listPaperByStudentId(Integer id);

    void submitScore(Integer paperId, List<Object> answerList);

    List<PaperQuestion> listPaperQuestionByPaperId(Integer id);

}
