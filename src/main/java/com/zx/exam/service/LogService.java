package com.zx.exam.service;

import com.zx.exam.entity.Logger;

import java.util.List;

/**
 * @ClassName LogService
 * @Author Sunny
 * @Date 2021年04月18日
 * @Package com.zx.exam.service
 * @Description TODO
 */
public interface LogService {
    int addLog(Logger logger);

    int deleteLog(Integer id);

    Logger selectByOperator(String name);

    List<Logger> listLog();

}
