package com.zx.exam.service;

import com.zx.exam.entity.Student;
import com.zx.exam.entity.Teacher;
import com.zx.exam.entity.select.StudentInfo;

import java.io.InputStream;
import java.util.List;

/**
 * @ClassName StudentService
 * @Author Sunny
 * @Date 2021年04月11日
 * @Package com.zx.exam.service
 * @Description TODO
 */
public interface StudentService {

    List<Student> getStudents();

    Integer addStudent(Student student);

    void deleteStudentById(Integer id);

    Student getStudentById(Integer id);
    void updateStudent(Student student);
    void forbidStudent(Integer id);
    void releaseStudent(Integer id);

    List<Student> getStudentsByKeyword(String key);


    List<Student> getStudentsByParam(String major,String grade,String stuClass);

    void importStudentsWithStream(InputStream inputStream);

    Student getByUserName(String userName);

    Integer updateStudentPassword(String password,String username);

    StudentInfo getStudentsInfo();

//    获取所有学生的id
    List<Integer> getStudentIdWithInfo(StudentInfo studentInfo);

}
