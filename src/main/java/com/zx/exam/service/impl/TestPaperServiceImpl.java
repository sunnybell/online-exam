package com.zx.exam.service.impl;


import com.alibaba.fastjson.JSONObject;
import com.zx.exam.entity.PaperQuestion;
import com.zx.exam.entity.Question;
import com.zx.exam.entity.Teacher;
import com.zx.exam.entity.TestPaper;
import com.zx.exam.entity.excel.ExcelStuScore;
import com.zx.exam.entity.requestbody.DeclareNewPaperRequestBody;
import com.zx.exam.entity.select.DeclaredPaper;
import com.zx.exam.entity.select.StuPaper;
import com.zx.exam.entity.select.StudentScore;
import com.zx.exam.entity.select.TestPaperInfoQuestion;
import com.zx.exam.mapper.TestPaperMapper;
import com.zx.exam.service.QuestionService;
import com.zx.exam.service.TestPaperService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpSession;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * @ClassName TestPaperServiceImpl
 * @Author Sunny
 * @Date 2021年05月12日
 * @Package com.zx.exam.service.impl
 * @Description 为试卷进行处理
 */
@Service
public class TestPaperServiceImpl implements TestPaperService {
    @Autowired
    TestPaperMapper testPaperMapper;
    @Autowired
    QuestionService questionService;

    /**
     * @param requestBody
     * @param session
     * 为每个选中的学生新建试卷
     */
    @Override
    public void declarePaperByParam(DeclareNewPaperRequestBody requestBody, HttpSession session) throws ParseException {
        List<Integer> stuIds = (List<Integer>) session.getAttribute("selectedStuId");
        Iterator<Integer> stuIdsIterator = stuIds.iterator();
        Teacher teacher = (Teacher) session.getAttribute("user");
        SimpleDateFormat sdf =   new SimpleDateFormat( "yyyy-MM-dd HH:mm" );

        randomSelectQuestion(requestBody.getQuestionParam(),0);

//        每个学生对应不同试卷
        while (stuIdsIterator.hasNext()){
            int studentId = stuIdsIterator.next();
            TestPaper testPaper = new TestPaper();
            testPaper.setTitle(requestBody.getTitle());
            testPaper.setStartTime(sdf.parse(requestBody.getStart()));
            testPaper.setEndTime(sdf.parse(requestBody.getEnd()));
            if (testPaper.getStartTime().compareTo(new Date()) < 0){
                testPaper.setSubmitTime(testPaper.getEndTime());
            }
            testPaper.setStudentId(studentId);
            testPaper.setTeacher(teacher.getName());
            testPaper.setPassScore(requestBody.getPassScore());
            testPaper.setSumScore(requestBody.getTotalScore());
            testPaper.setSubject(requestBody.getSubject());
            testPaper.setStudentScore("0");
            testPaperMapper.addPaper(testPaper);
            randomSelectQuestion(requestBody.getQuestionParam(),testPaper.getId());
        }
    }

    private void randomSelectQuestion(List<Map<String,String>> questionParam,Integer testPaperId){
        Map<String,String> single = questionParam.get(0); //单选
        Map<String,String> mutiple = questionParam.get(1); //多选
        Map<String,String> judge = questionParam.get(2);   //判断
        Map<String,String> shortAnswer = questionParam.get(3);   //简答
        System.out.println(questionParam);

        List<Integer> singleList = listByCatagory("单选",single);
        List<Integer> mutipleList = listByCatagory("多选",mutiple);
        List<Integer> judgeList = listByCatagory("判断",judge);
        List<Integer> shortAnswerList = listByCatagory("简答",shortAnswer);

        int indexInpaper = 1;

        indexInpaper = createPaperQuestion(single.get("scorePer"),singleList,testPaperId,indexInpaper);
        indexInpaper = createPaperQuestion(mutiple.get("scorePer"),mutipleList,testPaperId,indexInpaper);
        indexInpaper = createPaperQuestion(judge.get("scorePer"),judgeList,testPaperId,indexInpaper);
        createPaperQuestion(shortAnswer.get("scorePer"),shortAnswerList,testPaperId,indexInpaper);

    }

    private List<Integer> listByCatagory(String catagory,Map<String ,String> param){
        List<Integer> list = new ArrayList<>();
        List<Integer> simpleList = questionService.getQuestionIdByCondition(catagory,"简单",Integer.parseInt(param.get("simple")));
        list.addAll(simpleList);
        List<Integer> normalList = questionService.getQuestionIdByCondition(catagory,"一般",Integer.parseInt(param.get("normal")));
        list.addAll(normalList);
        List<Integer> diffList = questionService.getQuestionIdByCondition(catagory,"困难",Integer.parseInt(param.get("difficult")));
        list.addAll(diffList);
        return list;
    }


    private Integer createPaperQuestion(String score,List<Integer> selectInDatabase,Integer paperId,Integer indexInPaper){
        PaperQuestion paperQuestion = new PaperQuestion();
        for(int qId : selectInDatabase){
            paperQuestion.setIndex(indexInPaper);
            paperQuestion.setFullScore(score);
            paperQuestion.setTestpaperId(paperId);
            paperQuestion.setQuestionId(qId);
            testPaperMapper.addPaperQuestion(paperQuestion);
            indexInPaper++;
        }
        return indexInPaper;
    }

    @Override
    public TestPaper getDeclaredPaper() {
        return testPaperMapper.getDeclaredPaper();
    }

    @Override
    public TestPaper getById(Integer id) {
        return testPaperMapper.getById(id);
    }

    @Override
    public List<Question> getQuestionsByPaperId(Integer id) {
        return testPaperMapper.getQuestionsByPaperId(id);
    }

    @Override
    public List<DeclaredPaper> listPaper(String teacher) {
        List<TestPaper> testPapers = testPaperMapper.listPaper();
        SimpleDateFormat sdf =  new SimpleDateFormat( "yyyy年MM月dd日 HH:mm" );
        List<DeclaredPaper> declaredPapers = new ArrayList<>();
        for (TestPaper paper : testPapers){
            if (teacher.equals(paper.getTeacher())){
                DeclaredPaper declaredPaper = new DeclaredPaper();
                declaredPaper.setId(paper.getId());
                declaredPaper.setSubject(paper.getSubject());
                declaredPaper.setTeacher(paper.getTeacher());
                declaredPaper.setTitle(paper.getTitle());
                declaredPaper.setQuestionNum(testPaperMapper.getQuestionNumByPaperId(paper.getId()));
                declaredPaper.setScore(paper.getSumScore());
                declaredPaper.setStart(sdf.format(paper.getStartTime()));
                declaredPaper.setEnd(sdf.format(paper.getEndTime()));
                declaredPapers.add(declaredPaper);
            }
        }
        return declaredPapers;
    }

    @Override
    public List<StudentScore> listPaperScoreByName(String paperName) {
        return testPaperMapper.listPaperScoreByName(paperName);
    }

    @Override
    public TestPaper getByTitleAndStuId(String title, Integer stuId) {
        return testPaperMapper.getByTitleAndStuId(title,stuId);
    }

    @Override
    public List<TestPaperInfoQuestion> getQuestionInfoByPaperId(Integer id) {
        return testPaperMapper.getQuestionInfoByPaperId(id);
    }

    @Override
    public void markPaper(List<Object> shortMarkScores, String sum, Integer paperId) {
        testPaperMapper.updatePaperScoreById(sum,paperId);
        for (Object score : shortMarkScores){
            String te = JSONObject.toJSONString(score);
            JSONObject object = JSONObject.parseObject(te);
            testPaperMapper.updateQuestionScoreById((String) object.get("score"),Integer.parseInt((String) object.get("id")));
        }
    }

    @Override
    public List<ExcelStuScore> exportScoreByPaper(String paperTitle) {
        return testPaperMapper.exportScoreByPaper(paperTitle);
    }

    @Override
    public void deleteTestByName(String paperName) {
        List<TestPaper> paperList = testPaperMapper.listByPaperTitle(paperName);
        Iterator<TestPaper> iterator = paperList.iterator();
        while (iterator.hasNext()){
            testPaperMapper.deletePaperQuestionByPaperId(iterator.next().getId());
        }
        testPaperMapper.deletePaperByName(paperName);
    }

    @Override
    public List<StuPaper> listPaperByStudentId(Integer id) {
        List<TestPaper> testPapers = testPaperMapper.listPaperByStudentId(id);
        List<StuPaper> stuPaperList = new ArrayList<>();
        SimpleDateFormat sdf =  new SimpleDateFormat( "yyyy-MM-dd HH:mm" );
        Iterator<TestPaper> testPaperIterator = testPapers.iterator();
        while (testPaperIterator.hasNext()){
            TestPaper p = testPaperIterator.next();
            StuPaper stuPaper = new StuPaper();
            stuPaper.setId(p.getId());
            stuPaper.setTitle(p.getTitle());
            stuPaper.setScore(p.getStudentScore());
            stuPaper.setPassScore(p.getPassScore());
            stuPaper.setSumScore(p.getSumScore());
            if (p.getEndTime().compareTo(new Date()) < 0 || p.getSubmitTime() != null){
                stuPaper.setStatus("end");
            }else if (p.getStartTime().compareTo(new Date()) < 0 && p.getEndTime().compareTo(new Date()) > 0){
                stuPaper.setStatus("testing");
            }else {
                stuPaper.setStatus("begin");
            }
            stuPaper.setStartTime(sdf.format(p.getStartTime()));
            stuPaper.setEndTime(sdf.format(p.getEndTime()));
            stuPaper.setTeacherName(p.getTeacher());
            List<Question> questionList = testPaperMapper.getQuestionsByPaperId(p.getId());
            stuPaper.setQuestionCount(questionList.size());
            int selectCount = 0;
            int judgeCount = 0;
            int shortAnswerCount = 0;
            for (Question question : questionList){
                switch (question.getCatagory()){
                    case "简答":
                        shortAnswerCount ++;
                        break;
                    case "判断":
                        judgeCount ++;
                        break;
                    default:
                        selectCount ++;
                        break;
                }
            }
            stuPaper.setSelectCount(selectCount);
            stuPaper.setJudgeCount(judgeCount);
            stuPaper.setShorAnswerCount(shortAnswerCount);
            stuPaperList.add(stuPaper);
        }
        return stuPaperList;
    }

    @Override
    public void submitScore(Integer paperId, List<Object> answerList) {
        for (Object temp : answerList){
            String te = JSONObject.toJSONString(temp);
            JSONObject answer = JSONObject.parseObject(te);
            System.out.println(answer);
            Integer paperQuestionId = Integer.parseInt((String)answer.get("id"));
            String answerStr = (String) answer.get("answer");
            Question question = questionService.getQuestionById(testPaperMapper.getPaperQuestionById(paperQuestionId).getQuestionId());
            if (question.getAnswer().equals(answerStr)){
                testPaperMapper.setSubmitScore(paperQuestionId);
            }
            testPaperMapper.setSubmitAnswer(answerStr,paperQuestionId);
        }
        List<PaperQuestion> paperQuestions = testPaperMapper.listPaperQuestionByPaperId(paperId);
        int score = 0;
        for (PaperQuestion question : paperQuestions){
            score += Integer.parseInt(question.getScore());
        }
        testPaperMapper.updatePaperScoreById(String.valueOf(score),paperId);
        testPaperMapper.setPaperSubmitTimeById(paperId,new Date());
    }

    public List<PaperQuestion> listPaperQuestionByPaperId(Integer id){
        return testPaperMapper.listPaperQuestionByPaperId(id);
    }
}
