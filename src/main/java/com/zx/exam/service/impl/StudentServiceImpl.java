package com.zx.exam.service.impl;


import com.alibaba.excel.EasyExcel;
import com.alibaba.excel.read.builder.ExcelReaderSheetBuilder;
import com.zx.exam.entity.Student;
import com.zx.exam.entity.excel.ExcelQuestionModel;
import com.zx.exam.entity.excel.ExcelStudent;
import com.zx.exam.entity.requestbody.CreateQuestion;
import com.zx.exam.entity.select.StudentInfo;
import com.zx.exam.mapper.StudentMapper;
import com.zx.exam.service.StudentService;
import com.zx.exam.util.QuestionExcelListener;
import com.zx.exam.util.StudentExcelListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

/**
 * @ClassName StudentServiceImpl
 * @Author Sunny
 * @Date 2021年04月11日
 * @Package com.zx.exam.service.impl
 * @Description TODO
 */
@Service
public class StudentServiceImpl implements StudentService {

    @Autowired
    StudentMapper studentMapper;

    @Override
    public List<Student> getStudents() {
        return studentMapper.getStudents();
    }

    @Override
    public Integer addStudent(Student student) {
        return studentMapper.addStudent(student);
    }

    @Override
    public void deleteStudentById(Integer id) {
        studentMapper.deleteStudentById(id);
    }

    @Override
    public Student getStudentById(Integer id) {
        return studentMapper.getStudentById(id);
    }

    @Override
    public void updateStudent(Student student) {
        studentMapper.updateStudent(student);
    }

    @Override
    public void forbidStudent(Integer id) {
        studentMapper.forbidStudent(id);
    }

    @Override
    public void releaseStudent(Integer id) {
        studentMapper.releaseStudent(id);
    }

    @Override
    public List<Student> getStudentsByKeyword(String key) {
        return studentMapper.getStudentsByKeyword(key);
    }

    @Override
    public List<Student> getStudentsByParam(String major, String grade, String stuClass) {
        List<Student> students = studentMapper.getStudents();
        System.out.println(major+ " " +grade+ " " +stuClass);
        List<Student> result = new ArrayList<>();
        for (Student student : students){
            if (major.equals("全部") || major.equals(student.getMajor())){
                if (grade.equals("全部") || grade.equals(student.getGrade())){
                    if (stuClass.equals("全部") || stuClass.equals(student.getStuClass())){
                        result.add(student);
                    }
                }
            }
        }
        return result;
    }

    @Override
    public void importStudentsWithStream(InputStream inputStream) {
        StudentExcelListener listener = new StudentExcelListener();

        ExcelReaderSheetBuilder readerSheetBuilder = EasyExcel.read(inputStream, ExcelStudent.class,listener).sheet();
        readerSheetBuilder.doRead();

        for(Student student : listener.getList()){
            addStudent(student);
        }

    }

    @Override
    public Student getByUserName(String userName) {
        return studentMapper.getByUserName(userName);
    }

    @Override
    public Integer updateStudentPassword(String password, String username) {
        return studentMapper.updateStudentPassword(password,username);
    }

    @Override
    public StudentInfo getStudentsInfo() {
        StudentInfo info = new StudentInfo();
        info.setMajors(studentMapper.getMajors());
        info.setGrades(studentMapper.getGrades());
        info.setClasses(studentMapper.getClasses());
        return info;
    }

    @Override
    public List<Integer> getStudentIdWithInfo(StudentInfo studentInfo) {
        List<Student> studentList = studentMapper.getStudents();
        List<Integer> stuIdList = new ArrayList<>();
        for (Student student :studentList){
            if (studentInfo.getMajors().contains(student.getMajor()) || studentInfo.getMajors().size() == 0){
                if (studentInfo.getGrades().contains(student.getGrade()) || studentInfo.getGrades().size() == 0){
                    if (studentInfo.getClasses().contains(student.getStuClass()) || studentInfo.getClasses().size() == 0){
                        stuIdList.add(student.getId());
                    }
                }
            }
        }
        return stuIdList;
    }


}
