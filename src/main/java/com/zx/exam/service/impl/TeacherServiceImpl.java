package com.zx.exam.service.impl;


import com.zx.exam.entity.Teacher;
import com.zx.exam.mapper.TeacherMapper;
import com.zx.exam.service.TeacherService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @ClassName TeacherServiceImpl
 * @Author Sunny
 * @Date 2021年04月13日
 * @Package com.zx.exam.service.impl
 * @Description TODO
 */
@Service
public class TeacherServiceImpl implements TeacherService {
    @Autowired
    TeacherMapper teacherMapper;

    @Override
    public Teacher getByUserName(String userName) {
        return teacherMapper.getByUserName(userName);
    }

    @Override
    public Integer updateTeacherPassword(String password, String username) {
        return teacherMapper.updateTeacherPassword(password,username);
    }

    @Override
    public int addTeacher(Teacher teacher) {
        return teacherMapper.addTeacher(teacher);
    }

    @Override
    public List<Teacher> listTeacher() {
        return teacherMapper.listTeacher();
    }

    @Override
    public void deleteById(Integer id) {
        teacherMapper.deleteById(id);
    }

    @Override
    public Teacher getById(Integer id) {
        return teacherMapper.getById(id);
    }

    @Override
    public void updateTeacher(Teacher teacher) {
        teacherMapper.updateTeacher(teacher);
    }

    @Override
    public void forbidTeacher(Integer id) {
        teacherMapper.forbidTeacher(id);
    }

    @Override
    public void releaseTeacher(Integer id) {
        teacherMapper.releaseTeacher(id);
    }

    @Override
    public List<Teacher> getTeachersByKeyword(String key) {
        return teacherMapper.getTeachersByKeyword(key);
    }
}
