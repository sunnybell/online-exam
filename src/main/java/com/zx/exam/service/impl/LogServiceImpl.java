package com.zx.exam.service.impl;


import com.zx.exam.entity.Logger;
import com.zx.exam.mapper.LogMapper;
import com.zx.exam.service.LogService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @ClassName LogServiceImpl
 * @Author Sunny
 * @Date 2021年04月18日
 * @Package com.zx.exam.service.impl
 * @Description TODO
 */
@Service
public class LogServiceImpl implements LogService {

    @Autowired
    LogMapper logMapper;

    @Override
    public int addLog(Logger logger) {
        System.out.println("来了！"+ logger.toString());
        return logMapper.addLog(logger);
    }

    @Override
    public int deleteLog(Integer id) {
        return logMapper.deleteLog(id);
    }

    @Override
    public Logger selectByOperator(String name) {
        return logMapper.selectByOperator(name);
    }

    @Override
    public List<Logger> listLog() {
        return logMapper.listLog();
    }
}
