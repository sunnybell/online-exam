package com.zx.exam.service.impl;


import com.zx.exam.entity.Administrator;
import com.zx.exam.mapper.AdministratorMapper;
import com.zx.exam.service.AdministratorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @ClassName AdministratorServiceImpl
 * @Author Sunny
 * @Date 2021年04月13日
 * @Package com.zx.exam.service.impl
 * @Description TODO
 */
@Service
public class AdministratorServiceImpl implements AdministratorService {
    @Autowired
    AdministratorMapper administratorMapper;

    @Override
    public Administrator getByUserName(String userName) {
        return administratorMapper.getByUserName(userName);
    }

    @Override
    public void updatePassword(String password, String username) {
        administratorMapper.updatePasswordByUsername(password,username);
    }
}
