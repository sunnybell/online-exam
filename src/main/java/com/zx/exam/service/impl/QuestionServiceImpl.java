package com.zx.exam.service.impl;


import com.alibaba.excel.EasyExcel;
import com.alibaba.excel.read.builder.ExcelReaderSheetBuilder;
import com.zx.exam.entity.Question;
import com.zx.exam.entity.Subject;
import com.zx.exam.entity.excel.ExcelQuestionModel;
import com.zx.exam.entity.requestbody.CreateQuestion;
import com.zx.exam.entity.requestbody.Option;
import com.zx.exam.entity.requestbody.UpdateQuestion;
import com.zx.exam.mapper.QuestionMapper;
import com.zx.exam.service.QuestionService;
import com.zx.exam.util.QuestionExcelListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

/**
 * @ClassName QuestionServiceImpl
 * @Author Sunny
 * @Date 2021年04月27日
 * @Package com.zx.exam.service.impl
 * @Description TODO
 */
@Service
public class QuestionServiceImpl implements QuestionService {
    @Autowired
    QuestionMapper questionMapper;


    @Override
    public List<Integer> getQuestionIdByCondition(String catagory, String diff,Integer num) {
        return questionMapper.getQuestionIdByCondition(catagory,diff,num);
    }

    @Override
    public List<String> getAllSubjects() {
        return questionMapper.getAllSubjects();
    }

    /*添加试题*/
    @Override
    public void addQuestion(CreateQuestion createQuestion){
        Question question = new Question();
        question.setTitle(createQuestion.getTitle());
        question.setSubject(createQuestion.getSubject());
        question.setCatagory(createQuestion.getCatagory());
        question.setDifficulty(createQuestion.getDifficulty());
        question.setAnswer(createQuestion.getAnswer());
        if (createQuestion.getCatagory().equals("单选") || createQuestion.getCatagory().equals("多选")){
            List<Option> optionList = createQuestion.getOptions();
            question.setOptionA(optionList.get(0).getContent());
            question.setOptionB(optionList.get(1).getContent());
            question.setOptionC(optionList.get(2).getContent());
            question.setOptionD(optionList.get(3).getContent());
        }
        questionMapper.addQuestion(question);
    }

    @Override
    public List<Question> getAllQuestion() {
        return questionMapper.getAllQuestion();
    }

    @Override
    public Question getQuestionById(Integer questionId) {
       return questionMapper.getQuestionById(questionId);
    }


    @Override
    public void updateQuestion(UpdateQuestion questionUpdate) {
        int oldId = Integer.parseInt(questionUpdate.getId());
        questionMapper.deleteQuestionById(oldId);
        Question question = new Question();
        question.setTitle(questionUpdate.getTitle());
        question.setSubject(questionUpdate.getSubject());
        question.setAnswer(questionUpdate.getAnswer());
        question.setDifficulty(questionUpdate.getDifficulty());
        question.setCatagory(questionUpdate.getCatagory());

        List<Option> options = questionUpdate.getOptions();
        if (questionUpdate.getCatagory().equals("单选") || questionUpdate.getCatagory().equals("多选")){
            question.setOptionA(options.get(0).getContent());
            question.setOptionB(options.get(1).getContent());
            question.setOptionC(options.get(2).getContent());
            question.setOptionD(options.get(3).getContent());
        }
        questionMapper.addQuestion(question);
        questionMapper.updateQuestionId(question.getId(),oldId);
    }

    @Override
    public void deleteQuestionById(Integer id) {
        questionMapper.deleteQuestionById(id);
    }

    @Override
    public void importQuestionsWithStream(InputStream inputStream) {
        QuestionExcelListener listener = new QuestionExcelListener();

        ExcelReaderSheetBuilder readerSheetBuilder = EasyExcel.read(inputStream, ExcelQuestionModel.class,listener).sheet();
        readerSheetBuilder.doRead();

        for(CreateQuestion createQuestion : listener.getList()){
            addQuestion(createQuestion);
        }
    }

    @Override
    public List<Question> listQuestionByKey(String key) {
        return questionMapper.listQuestionByKey(key);
    }

    @Override
    public List<Question> listQuestionByParam(String catagory, String diff, String subject) {
        List<Question> questionSelectedList = questionMapper.getAllQuestion();

        List<Question> screenedQuestionsCata = new ArrayList<>();
        for (Question q : questionSelectedList){
            if (q.getCatagory().equals(catagory) || catagory.equals("全部")){
                screenedQuestionsCata.add(q);
            }
        }

        List<Question> screenedQuestionsDiff = new ArrayList<>();
        for (Question q :screenedQuestionsCata){
            if (q.getDifficulty().equals(diff) || diff.equals("全部")){
                screenedQuestionsDiff.add(q);
            }
        }

        List<Question> screenedQuestionsSub = new ArrayList<>();
        for (Question q :screenedQuestionsDiff){
            if (q.getSubject().equals(subject) || subject.equals("全部")){
                screenedQuestionsSub.add(q);
            }
        }
        System.out.println(screenedQuestionsSub);
        return screenedQuestionsSub;
    }


}
