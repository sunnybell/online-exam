package com.zx.exam.service;

import com.zx.exam.entity.Question;
import com.zx.exam.entity.Subject;
import com.zx.exam.entity.requestbody.CreateQuestion;
import com.zx.exam.entity.requestbody.UpdateQuestion;

import java.io.InputStream;
import java.util.List;

/**
 * @ClassName QuestionService
 * @Author Sunny
 * @Date 2021年04月27日
 * @Package com.zx.exam.service
 * @Description TODO
 */
public interface QuestionService {
    List<Integer> getQuestionIdByCondition(String catagory,String diff,Integer num);

    List<String> getAllSubjects();

    void addQuestion(CreateQuestion question);

    List<Question> getAllQuestion();

    Question getQuestionById(Integer questionId);

    void updateQuestion(UpdateQuestion question);

    void deleteQuestionById(Integer id);

    void importQuestionsWithStream(InputStream inputStream);

    List<Question> listQuestionByKey(String key);

    List<Question> listQuestionByParam(String catagory,String diff,String subject);

}
