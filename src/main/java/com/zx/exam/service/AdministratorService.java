package com.zx.exam.service;

import com.zx.exam.entity.Administrator;

/**
 * @ClassName AdministratorService
 * @Author Sunny
 * @Date 2021年04月13日
 * @Package com.zx.exam.service
 * @Description TODO
 */
public interface AdministratorService {
    Administrator getByUserName(String userName);

    void updatePassword(String password,String username);
}
