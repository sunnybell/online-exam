package com.zx.exam.common;

/**
 * @ClassName LoginStatusEnum
 * @Author Sunny
 * @Date 2021年04月12日
 * @Package com.zx.exam.common
 * @Description TODO
 */
public enum LoginStatusEnum {
    PWDCORRECT,
    PWDWRONG,
    LOGGED,
    VISITOR,
    FORBIDEN
}
