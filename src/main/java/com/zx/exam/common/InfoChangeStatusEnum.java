package com.zx.exam.common;

/**
 * @ClassName InfoChangeStatusEnum
 * @Author Sunny
 * @Date 2021年04月15日
 * @Package com.zx.exam.common
 * @Description TODO
 */
public enum InfoChangeStatusEnum {
    CHANGESECCESS,
    CHANGEFAIL,
    MATCHFAIL
}
